from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from bs4 import BeautifulSoup
import json
import requests

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

sheet_id = "1bZDdENMucVbe8ypDEFLzS2rnQtCuIoL6vZ2m8_UOTqg"

# The ID and range of a sample spreadsheet.
SAMPLE_RANGE_NAME1 = 'MLB Moneyline Odds!A3:AE'
SAMPLE_RANGE_NAME2 = 'MLB Spreads!A3:BC'
SAMPLE_RANGE_NAME3 = 'MLB Total!A3:BC'

class Espn:
    def __init__(self):
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        service = build('sheets', 'v4', credentials=creds)
        self.sheet = service.spreadsheets()
    def scrape(self):
        url = "http://www.espn.com/mlb/lines"
        source = requests.get(url).text
        soup = BeautifulSoup(source, 'lxml')
        heading = soup.find('h1',{'class':'h2'}).text
        p = heading.find("-")
        date1 = heading[p+1:len(heading)]
        container = soup.find('div',{'id':'my-teams-table'})
        table = container.find('table',{'class':'tablehead'})
        trs = table.findAll('tr',{'class':['stathead','oddrow','evenrow']})
        time1 = ''
        game = ''
        first_ml = ''
        second_ml = ''
        first_total = ''
        second_total = ''
        first_run = ''
        second_run = ''
        home = ''
        away = ''
        injur = ''
        pitcher = ''
        odd_rows = []
        flag = False
        batch_rows = []
        for tr in trs:
            class_name = ''
            try:
                class_name = tr["class"][0]
            except Exception:
                continue
            if class_name == "stathead":
                if flag == True:
                    item = {}
                    item["date"] = date1
                    item["time"] = time1
                    item["game"] = game
                    item["home"] = home.strip()
                    item["away"] = away.strip()
                    item["detail"] = odd_rows
                    item["injur"] = injur.strip()
                    item["pitcher"] = pitcher.strip()
                    batch_rows.append(item)
                    odd_rows = []
                    flag = False


                time1 = tr.text
                pos = time1.find(",")
                teamms = time1[0:pos]
                time1 = time1[pos+1:len(time1)]
                game = teamms
                team_array = teamms.split(" at ")
                away = team_array[0]
                home = team_array[1]

                flag = True

                continue
            a_tag = tr.find('a')
            if a_tag == None:
                tds = tr.findAll('td')
                fav = tds[0].text
                ml_txt = tds[1].decode_contents()

                ml_array = ml_txt.split("<br/>")
                first_ml = ml_array[0]
                pos1 = first_ml.find(":")
                first_ml = first_ml[pos1+1:len(first_ml)]
                first_ml = first_ml.strip()

                second_ml = ml_array[1]
                pos2 = second_ml.find(":")
                second_ml = second_ml[pos2+1:len(second_ml)]
                second_ml = second_ml.strip()

                tbs = tr.findAll('table',{'class':'tablehead'})
                tds = tbs[0].findAll('td')
                com = tds[0].text
                total = tds[1].decode_contents()
                total_array = total.split("<br/>")

                first_total = total_array[0]
                pos1 = first_total.find(":")
                first_total = first_total[pos1+1:len(first_total)]
                first_total = first_total.strip()
                first_total = com+" " + first_total

                second_total = total_array[1]
                pos2 = second_total.find(":")
                second_total = second_total[pos2+1:len(second_total)]
                second_total = second_total.strip()
                second_total = com+" " + second_total

                tds = tbs[1].findAll('td')
                runline = tds[1].decode_contents()
                run_arrary = runline.split("<br/>")

                first_run = run_arrary[0]
                pos1 = first_run.find(":")
                first_run = first_run[pos1+1:len(first_run)]
                first_run = first_run.strip()

                second_run = run_arrary[1]
                pos2 = second_run.find(":")
                second_run = second_run[pos2+1:len(second_run)]
                second_run = second_run.strip()

                odd_row = {}
                odd_row['fav'] = fav
                odd_row['first_ml'] = first_ml
                odd_row['second_ml'] = second_ml
                odd_row['first_total'] = first_total
                odd_row['second_total'] = second_total
                odd_row['first_run'] = first_run
                odd_row['second_run'] = second_run

                odd_rows.append(odd_row)
                print(odd_row)

            else:
                tds = tr.findAll("td")
                row_title = tds[0].text
                if row_title.find("Starting Pitchers") != -1:
                    pitcher = tds[1].text
                elif row_title.find("Injuries") != -1:
                    injur = tds[1].text

        item = {}
        item["date"] = date1
        item["time"] = time1
        item["home"] = home
        item["away"] = away
        item["game"] = game
        item["detail"] = odd_rows
        item["injur"] = injur
        item["pitcher"] = pitcher
        batch_rows.append(item)
        print(batch_rows)
        return batch_rows
    def start(self):
        batch_rows = self.scrape()
        values1 = []
        values2 = []
        values3 = []
        for row in batch_rows:
            date1 = row['date']
            time1 = row['time']
            game = row['game']
            home = row['home']
            away = row['away']
            injur = row['injur']
            pitcher = row['pitcher']
            details = row['detail']
            for detail in details:
                item = []
                item.append(date1)
                item.append(time1)
                item.append(game)
                item.append(home)
                item.append(away)
                item.append(detail['fav'])
                item.append(detail['first_ml'])
                item.append(injur)
                item.append(pitcher)
                values1.append(item)
                item[6] = detail['second_ml']
                values1.append(item)

                item[6] = detail['first_total']
                values3.append(item)
                item[6] = detail['second_total']
                values3.append(item)

                item[6] = detail['first_run']
                values2.append(item)
                item[6] = detail['second_run']
                values2.append(item)

        body1 = {
            'values':values1
        }
        self.sheet.values().append(spreadsheetId=sheet_id,range=SAMPLE_RANGE_NAME1,valueInputOption="USER_ENTERED", body=body1).execute()

        body2 = {
            'values':values2
        }
        self.sheet.values().append(spreadsheetId=sheet_id,range=SAMPLE_RANGE_NAME2,valueInputOption="USER_ENTERED", body=body2).execute()

        body3 = {
            'values':values3
        }
        self.sheet.values().append(spreadsheetId=sheet_id,range=SAMPLE_RANGE_NAME3,valueInputOption="USER_ENTERED", body=body3).execute()
def main():
    espn = Espn()
    espn.start()
    
if __name__ == "__main__":main()
