from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from oddsshark import Oddsshark
from espn import Espn
from vegas import Vegas
import time

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

sheet_id = "1bZDdENMucVbe8ypDEFLzS2rnQtCuIoL6vZ2m8_UOTqg"

# The ID and range of a sample spreadsheet.
SAMPLE_RANGE_NAME1 = 'MLB Schedule!A3:K'

class Start:
    def __init__(self):
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        service = build('sheets', 'v4', credentials=creds)
        self.sheet = service.spreadsheets()
    def start(self):

        while True:
            oddsshark = Oddsshark()
            oddsshark.change_line()
            value_list = []
            espn = Espn()
            result = espn.scrape()
            value_list = value_list + result
            vegas = Vegas()
            result = vegas.scrape()
            value_list = value_list + result
            print(value_list)
            body = {
                'values':value_list
            }
            self.sheet.values().clear(spreadsheetId=sheet_id, range=SAMPLE_RANGE_NAME1, body={}).execute()
            self.sheet.values().append(spreadsheetId=sheet_id,range=SAMPLE_RANGE_NAME1,valueInputOption="USER_ENTERED", body=body).execute()
            time.sleep(300)

def main():
    st = Start()
    st.start()
    
if __name__ == "__main__":main()
