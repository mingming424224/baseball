from bs4 import BeautifulSoup
import json
import requests


class Vegas:
    def __init__(self):
        pass
    def scrape(self):
        url = "http://www.vegasinsider.com/mlb/odds/las-vegas/"
        headers = {'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36','scheme':'https'}
        source = requests.get(url,headers=headers).text
        soup = BeautifulSoup(source, 'lxml')
        container = soup.find('table',{'class':'frodds-data-tbl'})
        #tbody = container.find('tbody')
        trs = container.findAll('tr')
        tr_len = len(trs)
        counter  = 0
        values = []
        while counter < tr_len:
            tds = trs[counter].findAll('td')
            teams = tds[0].findAll('a',{'class':'tabletext'})
            first_team = teams[0].text
            first_team = first_team.strip()
            second_team = teams[1].text
            second_team = second_team.strip()

            time1 = tds[0].find('span',{'class':'cellTextHot'}).text
            time1 = time1.strip()

            time_array =time1.split("  ")
            date1 = time_array[0]
            time1 = time_array[1]

            open_total = ''
            open_first_ml = ''
            open_second_ml = ''
            try:
                open_bet = tds[1].find('a').decode_contents()
                open_array = open_bet.split("<br/>")
                open_total = open_array[0]
                open_first_ml = open_array[1]
                open_second_ml = open_array[2]
            except Exception:
                pass

            vi_total = ''
            vi_first_ml = ''
            vi_second_ml = ''
            try:
                vi_bet = tds[2].find('a').decode_contents()
                vi_array = vi_bet.split("<br/>")
                vi_total = vi_array[0]
                vi_first_ml = vi_array[1]
                vi_second_ml = vi_array[2]
            except Exception:
                pass

            westgate_total = ''
            westgate_first_ml = ''
            westgate_second_ml = ''

            try:
                westgate_bet = tds[3].find('a').decode_contents()
                westgate_array = westgate_bet.split("<br/>")
                westgate_total = westgate_array[0]
                westgate_first_ml = westgate_array[1]
                westgate_second_ml = westgate_array[2]
            except Exception:
                pass

            mgm_total = ''
            mgm_first_ml = ''
            mgm_second_ml = ''

            try:
                mgm_bet = tds[4].find('a').decode_contents()
                mgm_array = mgm_bet.split("<br/>")
                mgm_total = mgm_array[0]
                mgm_first_ml = mgm_array[1]
                mgm_second_ml = mgm_array[2]
            except Exception:
                pass

            play_total = ''
            play_first_ml = ''
            play_second_ml = ''
            try:
                play_bet = tds[5].find('a').decode_contents()
                play_array = play_bet.split("<br/>")
                play_total = play_array[0]
                play_first_ml = play_array[1]
                play_second_ml = play_array[2]
            except Exception:
                pass

            will_total = ''
            will_first_ml = ''
            will_second_ml = ''
            try:
                will_bet = tds[6].find('a').decode_contents()
                will_array = will_bet.split("<br/>")
                will_total = will_array[0]
                will_first_ml = will_array[1]
                will_second_ml = will_array[2]
            except Exception:
                pass

            cg_total = ''
            cg_first_ml = ''
            cg_second_ml = ''
            try:
                cg_bet = tds[7].find('a').decode_contents()
                cg_array = cg_bet.split("<br/>")
                cg_total = cg_array[0]
                cg_first_ml = cg_array[1]
                cg_second_ml = cg_array[2]
            except Exception:
                pass

            circa_total = ''
            circa_first_ml = ''
            circa_second_ml = ''
            try:
                circa_bet = tds[8].find('a').decode_contents()
                circa_array = circa_bet.split("<br/>")
                circa_total = circa_array[0]
                circa_first_ml = circa_array[1]
                circa_second_ml = circa_array[2]
            except Exception:
                pass

            bet_total = ''
            bet_first_ml = ''
            bet_second_ml = ''
            try:
                bet_bet = tds[9].find('a').decode_contents()
                bet_array = bet_bet.split("<br/>")
                bet_total = bet_array[0]
                bet_first_ml = bet_array[1]
                bet_second_ml = bet_array[2]
            except Exception:
                pass

            counter += 1

            tds = trs[counter].findAll('td')
    #        border = trs[counter].find('td',{'class':'cellBorderR1'})
            pitcher = tds[0].text
            counter += 1
            
            tds = trs[counter].findAll('td')
            if len(tds) < 5:
                counter += 1

            item = []
            item.append(date1)
            item.append(time1)
            item.append("")
            item.append(first_team)
            item.append(second_team)
            item.append("Open")
            item.append(open_first_ml.strip())
            item.append(open_total.strip())
            item.append("")
            item.append("")
            item.append(pitcher.strip())
            item.append("vegasinsider")
            values.append(item)
            item[6] = open_second_ml.strip()
            values.append(item)

            item = []
            item.append(date1)
            item.append(time1)
            item.append("")
            item.append(first_team)
            item.append(second_team)
            item.append("VI Consensus")
            item.append(vi_first_ml.strip())
            item.append(vi_total.strip())
            item.append("")
            item.append("")
            item.append(pitcher.strip())
            item.append("vegasinsider")
            values.append(item)
            item[6] = vi_second_ml.strip()
            values.append(item)

            item = []
            item.append(date1)
            item.append(time1)
            item.append("")
            item.append(first_team)
            item.append(second_team)
            item.append("Westgate Superbook")
            item.append(westgate_first_ml.strip())
            item.append(westgate_total.strip())
            item.append("")
            item.append("")
            item.append(pitcher.strip())
            item.append("vegasinsider")
            values.append(item)
            item[6] = westgate_second_ml.strip()
            values.append(item)

            item = []
            item.append(date1)
            item.append(time1)
            item.append("")
            item.append(first_team)
            item.append(second_team)
            item.append("MGM Mirage")
            item.append(mgm_first_ml.strip())
            item.append(mgm_total.strip())
            item.append("")
            item.append("")
            item.append(pitcher.strip())
            item.append("vegasinsider")
            values.append(item)
            item[6] = mgm_second_ml.strip()
            values.append(item)

            item = []
            item.append(date1)
            item.append(time1)
            item.append("")
            item.append(first_team)
            item.append(second_team)
            item.append("playMGM")
            item.append(play_first_ml.strip())
            item.append(play_total.strip())
            item.append("")
            item.append("")
            item.append(pitcher.strip())
            item.append("vegasinsider")
            values.append(item)
            item[6] = play_second_ml.strip()
            values.append(item)

            item = []
            item.append(date1)
            item.append(time1)
            item.append("")
            item.append(first_team)
            item.append(second_team)
            item.append("William Hill")
            item.append(will_first_ml.strip())
            item.append(will_total.strip())
            item.append("")
            item.append("")
            item.append(pitcher.strip())
            item.append("vegasinsider")
            values.append(item)
            item[6] = will_second_ml.strip()
            values.append(item)

            item = []
            item.append(date1)
            item.append(time1)
            item.append("")
            item.append(first_team)
            item.append(second_team)
            item.append("CG Technology")
            item.append(cg_first_ml.strip())
            item.append(cg_total.strip())
            item.append("")
            item.append("")
            item.append(pitcher.strip())
            item.append("vegasinsider")
            values.append(item)
            item[6] = cg_second_ml.strip()
            values.append(item)

            item = []
            item.append(date1)
            item.append(time1)
            item.append("")
            item.append(first_team)
            item.append(second_team)
            item.append("Circa Sports")
            item.append(circa_first_ml.strip())
            item.append(circa_total.strip())
            item.append("")
            item.append("")
            item.append(pitcher.strip())
            item.append("vegasinsider")
            values.append(item)
            item[6] = circa_second_ml.strip()
            values.append(item)


            item = []
            item.append(date1)
            item.append(time1)
            item.append("")
            item.append(first_team)
            item.append(second_team)
            item.append("BetOnline")
            item.append(bet_first_ml.strip())
            item.append(bet_total.strip())
            item.append("")
            item.append("")
            item.append(pitcher.strip())
            item.append("vegasinsider")
            values.append(item)
            item[6] = bet_second_ml.strip()
            values.append(item)
            if date1.strip() == "":
                break
        return values

def main():
    vegas = Vegas()
    vegas.scrape()
    
if __name__ == "__main__":main()
