import requests
from bs4 import BeautifulSoup
import json
import time
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options 
from airtable import Airtable


class Upfolio:
    def __init__(self):
        with open("setting.json") as f:
            settings = json.load(f)
            api_key = settings["api key"]
            base_key = settings["classifications"]
            self.airtable = Airtable(base_key, "upfolio", api_key=api_key)
        chrome_options = Options()
        #    chrome_options.add_argument("--headless") 
        prefs = {'profile.managed_default_content_settings.images':2}
        chrome_options.add_experimental_option("prefs", prefs)
        chrome_path = r"chromedriver.exe" 
        self.driver = webdriver.Chrome(chrome_path,chrome_options=chrome_options)
        self.coin_list = []
    def clear_airtable(self):
        pages = self.airtable.get_all()
        for page in pages:
            self.airtable.delete(page["id"])
    def get_coins(self):
        url = "https://www.upfolio.com/"
        self.driver.get(url)

        soup = BeautifulSoup(self.driver.page_source, 'lxml')
        container = soup.find('div',{'id':'container'})
        coins = container.findAll('div',{'class':'mix','data-ix':'homehoverup'})
        for coin in coins[:len(coins)-1]:
            coin_name = coin.find('h2',{'class':'newcoinname'}).text
            a_tag = coin.find('a',{'class':'coinlinktop'})
            coin_item = {}
            coin_item["name"] = coin_name.strip()
            coin_item["link"] = a_tag["href"]
            self.coin_list.append(coin_item)
    def scrape_all(self):
        total = []
        for coin_item in self.coin_list:
            item = self.scrape_coin_page(coin_item["link"])
            item["name"] = coin_item["name"]
            total.append(item)
        push_rows = []
        for item in total:
            push_row = {}
            push_row["Name"] = item["name"]
            push_row["Live Price"] = float(item["current_price"])
            push_row["Today Price"] = float(item["daily_price"])
            push_row["Today Percentage"] = float(item["daily_percent"])/100
            push_row["Weekly Price"] = float(item["weekly_price"])
            push_row["Weekly Percentage"] = float(item["weekly_percent"])/100
            push_row["Monthly Price"] = float(item["monthly_price"])
            push_row["Monthly Percentage"] = float(item["monthly_percent"])/100
            push_rows.append(push_row)
        self.clear_airtable()
        self.airtable.batch_insert(push_rows)

    def scrape_coin_page(self,link):
        self.driver.get(link)
        time.sleep(4)
        soup = BeautifulSoup(self.driver.page_source, 'lxml')
        container = soup.find('div',{'class':'coindetailsection'})
        live_container = container.find('div',{'class':'live-price'})
        price_current = live_container.find('div',{'class':'price-current'})
        values = price_current.findAll('span',{'class':['odometer-value','odometer-formatting-mark']})
        item = {}
        cur_price = ''
        for value in values:
            cur_price +=value.text.strip()
        cur_price = cur_price.replace(",","")
        item["current_price"] = cur_price

        price_daily = live_container.find('div',{'class':'price-daily'})
        daily_values = price_daily.findAll('span',{'class':['odometer-value','odometer-formatting-mark']})
        daily_price = ''
        for daily_value in daily_values:
            daily_price += daily_value.text.strip()
        daily_percent = price_daily.find('div',{'class':'percentage'}).find('span').text

        daily_price = daily_price.replace(",","")

        daily_percent = daily_percent.replace("%","")
        item["daily_price"] = daily_price
        item["daily_percent"] = daily_percent

        price_weekly = live_container.find('div',{'class':'price-weekly'})
        weekly_values = price_weekly.findAll('span',{'class':['odometer-value','odometer-formatting-mark']})
        weekly_price = ''
        for weekly_value in weekly_values:
            weekly_price += weekly_value.text.strip()

        weekly_price = weekly_price.replace(",","")
        weekly_percentage = price_weekly.find('div',{'class':'percentage'}).find('span').text
        weekly_percentage = weekly_percentage.replace("%","")

        item["weekly_price"] = weekly_price
        item["weekly_percent"] = weekly_percentage

        price_monthly = live_container.find('div',{'class':'price-monthly'})
        monthly_values = price_monthly.findAll('span',{'class':['odometer-value','odometer-formatting-mark']})
        monthly_price = ''
        for monthly_value in monthly_values:
            monthly_price += monthly_value.text.strip()

        monthly_percentage = price_monthly.find('div',{'class':'percentage'}).find('span').text

        monthly_price = monthly_price.replace(",","")
        monthly_percentage = monthly_percentage.replace("%","")
        item["monthly_price"] = monthly_price
        item["monthly_percent"] = monthly_percentage

        return item

def main():
    upfolio = Upfolio()
    upfolio.get_coins()
    upfolio.scrape_all()
    
if __name__ == "__main__":main()
