import requests
from bs4 import BeautifulSoup
import json
import pyodbc
import pymssql
import decimal

class Messari:
    def __init__(self):
        with open("setting.json") as f:
            settings = json.load(f)
            db_name = settings["db name"]
            host = settings["host"]
            user = settings["user"]
            password = settings["password"]
            print(db_name)
            print(host)
            print(user)
            print(password)
            self.conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+host+';DATABASE='+db_name+';UID='+user+';PWD='+ password)
            self.excursor = self.conn.cursor()

            self.class_url = "https://api.messari.io/market/taxonomy"
            self.meta_url = "https://api.messari.io/market/assets/metadata"
            self.detail_url = "https://api.messari.io/market/assets/detailed"
    def scrape(self):
        # classifiers = requests.get(self.class_url).text
        # classifiers_json = json.loads(classifiers)
        meta = requests.get(self.meta_url).text
        meta_json = json.loads(meta)
        detail = requests.get(self.detail_url).text
        detail_json = json.loads(detail)
        assets = {}
        for item in detail_json["assets"]:
            item_json = {}
            item_json["name"] = item["name"]
            item_json["symbol"] = item["symbol"]
            try:
                item_json["priceUsd"] = float(item["priceUsd"])
            except Exception:
                item_json["priceUsd"]= ''
            try:
                item_json["percentageChange24HrUsd"] = float(item["percentageChange24HrUsd"])
            except Exception:
                item_json["percentageChange24HrUsd"] = ''
            try:
                item_json["realVol24HrUsd"] = float(item["realVol24HrUsd"])
            except Exception:
                item_json["realVol24HrUsd"] = ''
            try:
                item_json["y2050Marketcap"] = float(item["y2050Marketcap"])
            except Exception:
                item_json["y2050Marketcap"] = ''
            try:
                item_json["liquidMarketcap"] = float(item["liquidMarketcap"])
            except Exception:
                item_json["liquidMarketcap"] = ''
            try:        
                item_json["supplyPercentageIssued"] = float(item["supplyPercentageIssued"])
            except Exception:
                item_json["supplyPercentageIssued"] = ''
            try:
                item_json["percentageDownFromAth"] = float(item["percentageDownFromAth"])
            except Exception:
                item_json["percentageDownFromAth"] = ''
            try:
                item_json["flipsideRating"] = float(item["flipsideRating"])
            except Exception:
                item_json["flipsideRating"] = ''
            try:
                item_json["percentageChangeYtdUsd"] = float(item["percentageChangeYtdUsd"])
            except Exception:
                item_json["percentageChangeYtdUsd"] = ''
            assets[item["slug"]] = item_json

        self.excursor.execute("TRUNCATE TABLE dbo.admin_system_messari")
        for meta_item in meta_json["assets"]:
            slug = meta_item["slug"]
            temp_asset = assets[slug]
            print(meta_item["sectors"][0]["slug"])
            print(meta_item["categories"][0]["slug"])
            print("--------------")
            print(temp_asset["symbol"])
            print(temp_asset)
            SQLCommand = ("INSERT INTO dbo.admin_system_messari(name,symbol,category,subCategory,priceUsd,percentageChange24HrUsd,realVol24HrUsd,y2050Marketcap,liquidMarketcap,supplyPercentageIssued,percentageDownFromAth,flipsideRating,percentageChangeYtdUsd) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)")
            Values = [temp_asset["name"],temp_asset["symbol"],meta_item["categories"][0]["slug"],meta_item["sectors"][0]["slug"],temp_asset["priceUsd"],temp_asset["percentageChange24HrUsd"],temp_asset["realVol24HrUsd"],temp_asset["y2050Marketcap"],temp_asset["liquidMarketcap"],temp_asset["supplyPercentageIssued"],temp_asset["percentageDownFromAth"],temp_asset["flipsideRating"],temp_asset["percentageChangeYtdUsd"]]   
            self.excursor.execute(SQLCommand,Values) 
        self.conn.commit()

    def start(self):
        self.scrape()
        self.conn.close()

def main():
    messari = Messari()
    messari.start()
    
if __name__ == "__main__":main()

