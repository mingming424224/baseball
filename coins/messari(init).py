import requests
from bs4 import BeautifulSoup
import json
from datetime import datetime
import time
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options 

class Messari:
    def __init__(self):
        with open("setting.json") as f:
            settings = json.load(f)
            self.db_name = settings["db name"]
            self.host = settings["host"]
            self.user = settings["user"]
            self.password = settings["password"]

        chrome_options = Options()
#        chrome_options.add_argument("--headless") 
        prefs = {'profile.managed_default_content_settings.images':2}
        chrome_options.add_experimental_option("prefs", prefs)
        chrome_path = r"chromedriver.exe" 
        self.driver = webdriver.Chrome(chrome_path,chrome_options=chrome_options)
        self.url = "https://messari.io/onchainfx"
    def scrape(self):
        currency_data = []
        try:
            element_present = EC.presence_of_element_located((By.CLASS_NAME,"rt-tbody"))
            WebDriverWait(self.driver, 10).until(element_present)
            flag = True
            while flag == True:
                soup = BeautifulSoup(self.driver.page_source, 'lxml')
                tbody = soup.find('div',{'class':'rt-tbody'})
                rows = tbody.findAll('div',{'class':'rt-tr-group'})
                for row in rows:
                    item = []
                    tds = row.findAll('div',{'class':'rt-td'})
                    name = tds[1].find('a').text
                    usd_price = tds[3].text
                    hrvsusd = tds[4].text
                    real10_vol = tds[5].text
                    market_cap = tds[6].text
                    liquid_marketcap = tds[7].text
                    supply_issued = tds[8].text
                    down_from_ath = tds[9].text
                    rating = tds[10].text
                    year_to_date  = tds[11].text

                    item.append(name)
                    item.append(usd_price)
                    item.append(hrvsusd)
                    item.append(real10_vol)
                    item.append(market_cap)
                    item.append(liquid_marketcap)
                    item.append(supply_issued)
                    item.append(down_from_ath)
                    item.append(rating)
                    item.append(year_to_date)
                    currency_data.append(item)
                print(currency_data)
                pagination_class = soup.find('div',{'style':'display: block;'})['class']
                element = self.driver.find_element_by_class_name(pagination_class)
                self.driver.execute_script("arguments[0].setAttribute('style','display: none;')", element)
                element.click()
#                self.driver.find_element_by_xpath("""//*[@id="root"]/div/div[2]/div[2]/div[2]/div[3]/div[1]/div[2]/div/div[3]/button""").click()
                time.sleep(2)
                # try:
                # except Exception:
                #     flag = False

        except TimeoutException:
            print("timeout!")

    def start(self):
        self.driver.get(self.url)
        self.scrape()


    
def main():
    messari = Messari()
    messari.start()
    
if __name__ == "__main__":main()


