import requests
from bs4 import BeautifulSoup
import json
from airtable import Airtable
from threading import Timer
import time

class Messari:
    def __init__(self):
        with open("setting.json") as f:
            settings = json.load(f)
            api_key = settings["api key"]
            base_key = settings["classifications"]
            self.airtable = Airtable(base_key, "classifications", api_key=api_key)
            self.class_url = "https://api.messari.io/market/taxonomy"
            self.meta_url = "https://api.messari.io/market/assets/metadata"
            self.detail_url = "https://api.messari.io/market/assets/detailed"
    def clear_airtable(self):
        pages = self.airtable.get_all()
        for page in pages:
            self.airtable.delete(page["id"])

    def scrape(self):
        meta = requests.get(self.meta_url).text
        meta_json = json.loads(meta)
        detail = requests.get(self.detail_url).text
        detail_json = json.loads(detail)
        assets = {}
        for item in detail_json["assets"]:
            item_json = {}
            item_json["name"] = item["name"]
            item_json["symbol"] = item["symbol"]
            try:
                item_json["Price USD"] = round(float(item["priceUsd"]),2)
            except Exception:
                pass
            try:
                item_json["24hr vs USD"] = round(float(item["percentageChange24HrUsd"]),2)/100
            except Exception:
                pass
            try:
                item_json["Real 10 24hr Vol"] = int(item["vol24HrReal10Usd"])
            except Exception:
                pass
            try:
                item_json["Y2050 Marketcap"] = int(item["y2050Marketcap"])
            except Exception:
                pass
            try:
                item_json["Liquid Marketcap"] = int(item["liquidMarketcap"])
            except Exception:
                pass
            try:        
                item_json["Supply % Issued"] = round(float(item["supplyPercentageIssued"]),2)/100
            except Exception:
                pass
            try:
                item_json["down from ATH"] = int(item["percentageDownFromAth"])/100
            except Exception:
                pass
            try:
                item_json["Flipside FCAS Ratting"] = int(item["flipsideFcasValue"])
            except Exception:
                pass
            try:
                item_json["Year-toDate(USD)"] = round(float(item["percentageChangeYtdUsd"]),2)/100
            except Exception:
                pass
            assets[item["slug"]] = item_json
        total = []
        for meta_item in meta_json["assets"]:
            slug = meta_item["slug"]
            temp_asset = assets[slug]
            print(temp_asset["symbol"])
            temp_asset["Category"] = meta_item["categories"][0]["slug"]
            temp_asset["Sub Category"] = meta_item["sectors"][0]["slug"]
            print(temp_asset)
            total.append(temp_asset)
        self.clear_airtable()
        self.airtable.batch_insert(total)
    def start(self):
        self.scrape()

