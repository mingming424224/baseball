import requests
from bs4 import BeautifulSoup
import json
from airtable import Airtable
import time

class TradingView:
    def __init__(self):
        with open("setting.json") as f:
            settings = json.load(f)
            api_key = settings["api key"]
            base_key = settings["classifications"]

            self.airtable_osc = Airtable(base_key, "tradingview(OSCILLATORS)", api_key=api_key)
            self.airtable_mov = Airtable(base_key, "tradingview(MOVING AVERAGES)", api_key=api_key)
            self.airtable_piv = Airtable(base_key, "tradingview(PIVOTS)", api_key=api_key)

            self.url = "https://scanner.tradingview.com/crypto/scan"
            self.headers = {'content-type':'application/x-www-form-urlencoded','Origin':'https://www.tradingview.com','Referer':'https://www.tradingview.com/symbols/LTCUSD/technicals/','User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'}
            self.hour_list = [1,5,15,60,240,-1,-2,-3]
            self.time_frames = ["1 minute","5 minutes","15 minutes","1 hour","4 hours","1 day","1 week","1 month"]
            self.day = '{"symbols":{"tickers":["BITSTAMP:LTCUSD"],"query":{"types":[]}},"columns":["Recommend.Other","Recommend.All","Recommend.MA","RSI","RSI[1]","Stoch.K","Stoch.D","Stoch.K[1]","Stoch.D[1]","CCI20","CCI20[1]","ADX","ADX+DI","ADX-DI","ADX+DI[1]","ADX-DI[1]","AO","AO[1]","Mom","Mom[1]","MACD.macd","MACD.signal","Rec.Stoch.RSI","Stoch.RSI.K","Rec.WR","W.R","Rec.BBPower","BBPower","Rec.UO","UO","EMA5","close","SMA5","EMA10","SMA10","EMA20","SMA20","EMA30","SMA30","EMA50","SMA50","EMA100","SMA100","EMA200","SMA200","Rec.Ichimoku","Ichimoku.BLine","Rec.VWMA","VWMA","Rec.HullMA9","HullMA9","Pivot.M.Classic.S3","Pivot.M.Classic.S2","Pivot.M.Classic.S1","Pivot.M.Classic.Middle","Pivot.M.Classic.R1","Pivot.M.Classic.R2","Pivot.M.Classic.R3","Pivot.M.Fibonacci.S3","Pivot.M.Fibonacci.S2","Pivot.M.Fibonacci.S1","Pivot.M.Fibonacci.Middle","Pivot.M.Fibonacci.R1","Pivot.M.Fibonacci.R2","Pivot.M.Fibonacci.R3","Pivot.M.Camarilla.S3","Pivot.M.Camarilla.S2","Pivot.M.Camarilla.S1","Pivot.M.Camarilla.Middle","Pivot.M.Camarilla.R1","Pivot.M.Camarilla.R2","Pivot.M.Camarilla.R3","Pivot.M.Woodie.S3","Pivot.M.Woodie.S2","Pivot.M.Woodie.S1","Pivot.M.Woodie.Middle","Pivot.M.Woodie.R1","Pivot.M.Woodie.R2","Pivot.M.Woodie.R3","Pivot.M.Demark.S1","Pivot.M.Demark.Middle","Pivot.M.Demark.R1"]}'
            self.week = '{"symbols":{"tickers":["BITSTAMP:LTCUSD"],"query":{"types":[]}},"columns":["Recommend.Other|1W","Recommend.All|1W","Recommend.MA|1W","RSI|1W","RSI[1]|1W","Stoch.K|1W","Stoch.D|1W","Stoch.K[1]|1W","Stoch.D[1]|1W","CCI20|1W","CCI20[1]|1W","ADX|1W","ADX+DI|1W","ADX-DI|1W","ADX+DI[1]|1W","ADX-DI[1]|1W","AO|1W","AO[1]|1W","Mom|1W","Mom[1]|1W","MACD.macd|1W","MACD.signal|1W","Rec.Stoch.RSI|1W","Stoch.RSI.K|1W","Rec.WR|1W","W.R|1W","Rec.BBPower|1W","BBPower|1W","Rec.UO|1W","UO|1W","EMA5|1W","close|1W","SMA5|1W","EMA10|1W","SMA10|1W","EMA20|1W","SMA20|1W","EMA30|1W","SMA30|1W","EMA50|1W","SMA50|1W","EMA100|1W","SMA100|1W","EMA200|1W","SMA200|1W","Rec.Ichimoku|1W","Ichimoku.BLine|1W","Rec.VWMA|1W","VWMA|1W","Rec.HullMA9|1W","HullMA9|1W","Pivot.M.Classic.S3|1W","Pivot.M.Classic.S2|1W","Pivot.M.Classic.S1|1W","Pivot.M.Classic.Middle|1W","Pivot.M.Classic.R1|1W","Pivot.M.Classic.R2|1W","Pivot.M.Classic.R3|1W","Pivot.M.Fibonacci.S3|1W","Pivot.M.Fibonacci.S2|1W","Pivot.M.Fibonacci.S1|1W","Pivot.M.Fibonacci.Middle|1W","Pivot.M.Fibonacci.R1|1W","Pivot.M.Fibonacci.R2|1W","Pivot.M.Fibonacci.R3|1W","Pivot.M.Camarilla.S3|1W","Pivot.M.Camarilla.S2|1W","Pivot.M.Camarilla.S1|1W","Pivot.M.Camarilla.Middle|1W","Pivot.M.Camarilla.R1|1W","Pivot.M.Camarilla.R2|1W","Pivot.M.Camarilla.R3|1W","Pivot.M.Woodie.S3|1W","Pivot.M.Woodie.S2|1W","Pivot.M.Woodie.S1|1W","Pivot.M.Woodie.Middle|1W","Pivot.M.Woodie.R1|1W","Pivot.M.Woodie.R2|1W","Pivot.M.Woodie.R3|1W","Pivot.M.Demark.S1|1W","Pivot.M.Demark.Middle|1W","Pivot.M.Demark.R1|1W"]}'
            self.month = '{"symbols":{"tickers":["BITSTAMP:LTCUSD"],"query":{"types":[]}},"columns":["Recommend.Other|1M","Recommend.All|1M","Recommend.MA|1M","RSI|1M","RSI[1]|1M","Stoch.K|1M","Stoch.D|1M","Stoch.K[1]|1M","Stoch.D[1]|1M","CCI20|1M","CCI20[1]|1M","ADX|1M","ADX+DI|1M","ADX-DI|1M","ADX+DI[1]|1M","ADX-DI[1]|1M","AO|1M","AO[1]|1M","Mom|1M","Mom[1]|1M","MACD.macd|1M","MACD.signal|1M","Rec.Stoch.RSI|1M","Stoch.RSI.K|1M","Rec.WR|1M","W.R|1M","Rec.BBPower|1M","BBPower|1M","Rec.UO|1M","UO|1M","EMA5|1M","close|1M","SMA5|1M","EMA10|1M","SMA10|1M","EMA20|1M","SMA20|1M","EMA30|1M","SMA30|1M","EMA50|1M","SMA50|1M","EMA100|1M","SMA100|1M","EMA200|1M","SMA200|1M","Rec.Ichimoku|1M","Ichimoku.BLine|1M","Rec.VWMA|1M","VWMA|1M","Rec.HullMA9|1M","HullMA9|1M","Pivot.M.Classic.S3|1M","Pivot.M.Classic.S2|1M","Pivot.M.Classic.S1|1M","Pivot.M.Classic.Middle|1M","Pivot.M.Classic.R1|1M","Pivot.M.Classic.R2|1M","Pivot.M.Classic.R3|1M","Pivot.M.Fibonacci.S3|1M","Pivot.M.Fibonacci.S2|1M","Pivot.M.Fibonacci.S1|1M","Pivot.M.Fibonacci.Middle|1M","Pivot.M.Fibonacci.R1|1M","Pivot.M.Fibonacci.R2|1M","Pivot.M.Fibonacci.R3|1M","Pivot.M.Camarilla.S3|1M","Pivot.M.Camarilla.S2|1M","Pivot.M.Camarilla.S1|1M","Pivot.M.Camarilla.Middle|1M","Pivot.M.Camarilla.R1|1M","Pivot.M.Camarilla.R2|1M","Pivot.M.Camarilla.R3|1M","Pivot.M.Woodie.S3|1M","Pivot.M.Woodie.S2|1M","Pivot.M.Woodie.S1|1M","Pivot.M.Woodie.Middle|1M","Pivot.M.Woodie.R1|1M","Pivot.M.Woodie.R2|1M","Pivot.M.Woodie.R3|1M","Pivot.M.Demark.S1|1M","Pivot.M.Demark.Middle|1M","Pivot.M.Demark.R1|1M"]}'
    def scrape(self,hours):   
        json_payload = None
        if hours >0:
            payload = f'"Recommend.Other|{hours}","Recommend.All|{hours}","Recommend.MA|{hours}","RSI|{hours}","RSI[1]|{hours}","Stoch.K|{hours}","Stoch.D|{hours}","Stoch.K[1]|{hours}","Stoch.D[1]|{hours}","CCI20|{hours}","CCI20[1]|{hours}","ADX|{hours}","ADX+DI|{hours}","ADX-DI|{hours}","ADX+DI[1]|{hours}","ADX-DI[1]|{hours}","AO|{hours}","AO[1]|{hours}","Mom|{hours}","Mom[1]|{hours}","MACD.macd|{hours}","MACD.signal|{hours}","Rec.Stoch.RSI|{hours}","Stoch.RSI.K|{hours}","Rec.WR|{hours}","W.R|{hours}","Rec.BBPower|{hours}","BBPower|{hours}","Rec.UO|{hours}","UO|{hours}","EMA5|{hours}","close|{hours}","SMA5|{hours}","EMA10|{hours}","SMA10|{hours}","EMA20|{hours}","SMA20|{hours}","EMA30|{hours}","SMA30|{hours}","EMA50|{hours}","SMA50|{hours}","EMA100|{hours}","SMA100|{hours}","EMA200|{hours}","SMA200|{hours}","Rec.Ichimoku|{hours}","Ichimoku.BLine|{hours}","Rec.VWMA|{hours}","VWMA|{hours}","Rec.HullMA9|{hours}","HullMA9|{hours}","Pivot.M.Classic.S3|{hours}","Pivot.M.Classic.S2|{hours}","Pivot.M.Classic.S1|{hours}","Pivot.M.Classic.Middle|{hours}","Pivot.M.Classic.R1|{hours}","Pivot.M.Classic.R2|{hours}","Pivot.M.Classic.R3|{hours}","Pivot.M.Fibonacci.S3|{hours}","Pivot.M.Fibonacci.S2|{hours}","Pivot.M.Fibonacci.S1|{hours}","Pivot.M.Fibonacci.Middle|{hours}","Pivot.M.Fibonacci.R1|{hours}","Pivot.M.Fibonacci.R2|{hours}","Pivot.M.Fibonacci.R3|{hours}","Pivot.M.Camarilla.S3|{hours}","Pivot.M.Camarilla.S2|{hours}","Pivot.M.Camarilla.S1|{hours}","Pivot.M.Camarilla.Middle|{hours}","Pivot.M.Camarilla.R1|{hours}","Pivot.M.Camarilla.R2|{hours}","Pivot.M.Camarilla.R3|{hours}","Pivot.M.Woodie.S3|{hours}","Pivot.M.Woodie.S2|{hours}","Pivot.M.Woodie.S1|{hours}","Pivot.M.Woodie.Middle|{hours}","Pivot.M.Woodie.R1|{hours}","Pivot.M.Woodie.R2|{hours}","Pivot.M.Woodie.R3|{hours}","Pivot.M.Demark.S1|{hours}","Pivot.M.Demark.Middle|{hours}","Pivot.M.Demark.R1|{hours}"'
            json_str = '{"symbols":{"tickers":["BITSTAMP:LTCUSD"],"query":{"types":[]}},"columns":[' + payload + ']}'
            json_payload = json.loads(json_str)
        elif hours == -1:
            json_payload = json.loads(self.day)
        elif hours == -2:
            json_payload = json.loads(self.week)
        elif hours == -3:
            json_payload = json.loads(self.month)
        response = requests.post(self.url,json=json_payload,headers=self.headers).text
        res_json = json.loads(response)
        return res_json["data"][0]["d"]
    def decide_summary(self,value):
        value_float = float(value)
        state = ""
        if value_float == 0:
            state = "NEUTRAL"
        elif value_float <= -0.5:
            state = "STRONG SELL"
        elif value_float > -0.5 and value < 0:
            state = "SELL"
        elif value_float <= 0.5:
            state = "BUY"
        elif value_float > 0.5:
            state = "STRONG BUY"
        return state

    def clear_airtable(self):
        pages = self.airtable_osc.get_all()
        for page in pages:
            self.airtable_osc.delete(page["id"])
        pages = self.airtable_mov.get_all()
        for page in pages:
            self.airtable_mov.delete(page["id"])
        pages = self.airtable_piv.get_all()
        for page in pages:
            self.airtable_piv.delete(page["id"])

    def start(self):
        osc_list = []
        mov_list = []
        piv_list = []
        counter = 0
        for hour in self.hour_list:
            res = self.scrape(hour)
            summary = {}
            osc = {}
            osc["timeframe"] = self.time_frames[counter]
            osc_decide = self.decide_summary(res[0])
            sum_decide = self.decide_summary(res[1])
            mov_decide = self.decide_summary(res[2])
            osc["oscillators"] = osc_decide
            osc["summary"] = sum_decide
            osc["moving averages"] = mov_decide
            try:
                osc["Relative Strength Index"] = float(res[3])
            except Exception:
                pass

            try:
                osc["Stochastic %K"] = float(res[5])
            except Exception:
                pass
            try:
                osc["Commodity Channel Index"] = float(res[9])
            except Exception:
                pass
            try:
                osc["Average Directional Index"] = float(res[11])
            except Exception:
                pass
            try:
                osc["Awesome Oscillator"] = float(res[16])
            except Exception:
                pass
            try:
                osc["Momentum"] = float(res[18])
            except Exception:
                pass
            try:
                osc["MACD Level"] = float(res[20])
            except Exception:
                pass
            try:
                osc["Stochastic RSI Fast"] = float(res[23])
            except Exception:
                pass
            try:
                osc["Williams Percent Range"] = float(res[25])
            except Exception:
                pass
            try:
                osc["Bull Bear Power"] = float(res[27])
            except Exception:
                pass
            try:
                osc["Ultimate Oscillator"] = float(res[29])
            except Exception:
                pass
            osc_list.append(osc)
            mov= {}
            mov["timeframe"] = self.time_frames[counter]
            mov["oscillators"] = osc_decide
            mov["summary"] = sum_decide
            mov["moving averages"] = mov_decide
            try:
                mov["EMV(5)"] = float(res[30])
            except Exception:
                pass
            try:
                mov["SMV(5)"] = float(res[32])
            except Exception:
                pass
            try:
                mov["EMV(10)"] = float(res[33])
            except Exception:
                pass
            try:
                mov["SMV(10)"] = float(res[34])
            except Exception:
                pass
            try:
                mov["EMV(20)"] = float(res[35])
            except Exception:
                pass
            try:
                mov["SMV(20)"] = float(res[36])
            except Exception:
                pass
            try:
                mov["EMV(30)"] = float(res[37])
            except Exception:
                pass
            try:
                mov["SMV(30)"] = float(res[38])
            except Exception:
                pass
            try:
                mov["EMV(50)"] = float(res[39])
            except Exception:
                pass
            try:
                mov["SMV(50)"] = float(res[40])
            except Exception:
                pass
            try:
                mov["EMV(100)"] = float(res[41])
            except Exception:
                pass
            try:
                mov["SMV(100)"] = float(res[42])
            except Exception:
                pass
            try:
                mov["EMV(200)"] = float(res[43])
            except Exception:
                pass
            try:
                mov["SMV(200)"] = float(res[44])
            except Exception:
                pass
            try:
                mov["ICBL"] = float(res[46])
            except Exception:
                pass
            try:
                mov["VWMA"] = float(res[48])
            except Exception:
                pass
            try:
                mov["HMA"] = float(res[50])
            except Exception:
                pass
            mov_list.append(mov)

            pivot = ["S3","S2","S1","P","R1","R2","R3"]
            classic = []
            fib = []
            cam = []
            wood = []
            dm = []
            for item in res[51:58]:
                classic.append(item)
            for item in res[58:65]:
                fib.append(item)
            for item in res[65:72]:
                cam.append(item)
            for item in res[72:79]:
                wood.append(item)
            for item in res[79:82]:
                dm.append
            print(classic)
            print(fib)
            print(cam)
            print(wood)
            print(dm)
            for count in range(0,7):
                piv = {}
                piv["timeframe"] = self.time_frames[counter]
                piv["Pivot"] = pivot[count]
                piv["Classic"] = classic[count]
                piv["Fibonacci"] = fib[count]
                piv["Camarilla"] = cam[count]
                piv["Woodie"] = wood[count]
                if count > 1 and count < 5:
                    piv["DM"] = wood[count-2]
                piv_list.append(piv)
            counter += 1
        self.clear_airtable()
        self.airtable_osc.batch_insert(osc_list)
        self.airtable_mov.batch_insert(mov_list)
        self.airtable_piv.batch_insert(piv_list)

