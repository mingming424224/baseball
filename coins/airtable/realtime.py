import six
from classifier_air import Messari
from trading_air import TradingView
import time

while True:
    messari = Messari()
    messari.start()

    tradingView = TradingView()
    tradingView.start()
    time.sleep(60)
