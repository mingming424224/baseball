import requests
from bs4 import BeautifulSoup
import json
import time
import six
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options 
from airtable import Airtable


class Upfolio:
    def __init__(self):
        with open("setting.json") as f:
            settings = json.load(f)
            api_key = settings["api key"]
            base_key = settings["classifications"]
            self.airtable = Airtable(base_key, "upfolio", api_key=api_key)
        chrome_options = Options()
        #chrome_options.add_argument("--headless") 
        prefs = {'profile.managed_default_content_settings.images':2}
        chrome_options.add_experimental_option("prefs", prefs)
        chrome_path = r"chromedriver.exe" 
        self.driver = webdriver.Chrome(chrome_path,chrome_options=chrome_options)
        self.coin_list = []
    def clear_airtable(self):
        pages = self.airtable.get_all()
        for page in pages:
            self.airtable.delete(page["id"])
    def get_coins(self):
        url = "https://www.upfolio.com/"
        self.driver.get(url)

        soup = BeautifulSoup(self.driver.page_source, 'lxml')
        container = soup.find('div',{'id':'container'})
        coins = container.findAll('div',{'class':'mix','data-ix':'homehoverup'})
        for coin in coins[:len(coins)-1]:
            coin_name = coin.find('h2',{'class':'newcoinname'}).text
            a_tag = coin.find('a',{'class':'coinlinktop'})
            coin_item = {}
            coin_item["name"] = coin_name.strip()
            coin_item["link"] = a_tag["href"]
            self.coin_list.append(coin_item)
    def scrape_all(self):
        total = []
        for coin_item in self.coin_list:
            item = self.scrape_coin_page(coin_item["link"])
            item["name"] = coin_item["name"]
            total.append(item)
            print(item)

        push_rows = []
        for item in total:
            print(item)
            push_row = {}
            push_row["Name"] = item["name"]
            try:
                push_row["Live Price"] = float(item["current_price"])
            except Exception:
                push_row["Live Price"] = 0
            try:
                push_row["Today Price"] = float(item["daily_price"])
            except Exception:
                push_row["Today Price"] = 0

            try:
                push_row["Today Percentage"] = float(item["daily_percent"])/100
            except Exception:
                push_row["Today Percentage"] = 0
            try:
                push_row["Weekly Price"] = float(item["weekly_price"])
            except Exception:
                push_row["Weekly Price"] = 0
            try:
                push_row["Weekly Percentage"] = float(item["weekly_percent"])/100
            except Exception:
                push_row["Weekly Percentage"] = 0
            try:
                push_row["Monthly Price"] = float(item["monthly_price"])
            except Exception:
                push_row["Monthly Price"] = 0
            try:
                push_row["Monthly Percentage"] = float(item["monthly_percent"])/100
            except Exception:
                push_row["Monthly Percentage"] = 0
            push_row["Coin Type"] = item["coin_type"]
            push_row["Website URL"] = item["website url"]
            push_row["Release"] = item["release"]
            push_row["Reddit URL"] = item["reddit"]
            push_row["Twitter URL"] = item["twitter"]
            push_row["Facebook URL"] = item["facebook"]
            push_row["Coin Summary"] = item["coin summary"]
            push_row["Market Cap"] = item["market cap"]
            push_row["24H Volume"] = item["24h volume"]
            push_row["Circulation Supply"] = item["circulation supply"]
            push_row["Max. Supply"] = item["max supply"]
            push_row["Released"] = item["Released"]
            push_row["Use Cases Collections"] = item["use case collections"]
            push_row["Social Score"] = item["social score"]
            push_row["Reddit Subscribers"] = item["reddit sub"]
            push_row["Reddit Percentage"] = item["reddit pct"]
            push_row["Twitter Followers"] = item["twitter followers"]
            push_row["Twitter Percentage"] = item["twitter pct"]
            push_row["Facebook Likes"] = item["facebook likes"]
            push_row["Facebook Percentage"] = item["facebook pct"]
            push_row["Team"] = item["team"]
            push_row["Amount Raised"] = item["amount raised"]
            push_row["ICO Bonus"] = item["ico bonus"]
            push_row["Min. Investment"] = item["min investment"]
            push_row["Founding Target"] = item["founding target"]
            push_row["Founding Cap"] = item["founding cap"]
            push_row["Country"] = item["country"]
            push_row["Proof Type"] = item["proof type"]
            push_row["Platform"] = item["platform"]
            push_row["Mobile Wallet"] = item["mobile wallet"]
            push_row["Web Wallet"] = item["web wallet"]
            push_row["Hardware Wallet"] = item["hardware wallet"]
            
            push_rows.append(push_row)
        self.clear_airtable()
        self.airtable.batch_insert(push_rows)

    def scrape_coin_page(self,link):
        self.driver.get(link)
        time.sleep(3)
        soup = BeautifulSoup(self.driver.page_source, 'lxml')
        container = soup.find('div',{'class':'coindetailsection'})
        live_container = container.find('div',{'class':'live-price'})
        price_current = live_container.find('div',{'class':'price-current'})
        values = price_current.findAll('span',{'class':['odometer-value','odometer-formatting-mark']})
        item = {}
        cur_price = ''
        for value in values:
            cur_price +=value.text.strip()
        cur_price = cur_price.replace(",","")
        item["current_price"] = cur_price

        price_daily = live_container.find('div',{'class':'price-daily'})
        daily_values = price_daily.findAll('span',{'class':['odometer-value','odometer-formatting-mark']})
        daily_price = ''
        if daily_values:
            for daily_value in daily_values:
                daily_price += daily_value.text.strip()
        else:
            daily_price = '0'
        daily_percent = price_daily.find('div',{'class':'percentage'}).find('span')
        daily_percentage = '0'
        if daily_percent:
            daily_percentage = daily_percent.text
            daily_percentage = daily_percentage.replace("%","")

        daily_price = daily_price.replace(",","")

        item["daily_price"] = daily_price
        item["daily_percent"] = daily_percentage

        price_weekly = live_container.find('div',{'class':'price-weekly'})
        weekly_values = price_weekly.findAll('span',{'class':['odometer-value','odometer-formatting-mark']})
        weekly_price = ''
        if weekly_values:
            for weekly_value in weekly_values:
                weekly_price += weekly_value.text.strip()
        else:
            weekly_price = '0'

        weekly_price = weekly_price.replace(",","")
        weekly_percent = price_weekly.find('div',{'class':'percentage'}).find('span')
        weekly_percentage = ''
        if weekly_percent:
            weekly_percentage = weekly_percent.text
            weekly_percentage = weekly_percentage.replace("%","")
        else:
            weekly_percentage = '0'

        item["weekly_price"] = weekly_price
        item["weekly_percent"] = weekly_percentage

        price_monthly = live_container.find('div',{'class':'price-monthly'})
        monthly_values = price_monthly.findAll('span',{'class':['odometer-value','odometer-formatting-mark']})
        monthly_price = ''
        
        if monthly_values:
            for monthly_value in monthly_values:
                monthly_price += monthly_value.text.strip()

        monthly_percent = price_monthly.find('div',{'class':'percentage'}).find('span')
        monthly_percentage = ''
        if monthly_percent:
            monthly_percentage = monthly_percent.text
            monthly_percentage = monthly_percentage.replace("%","")
        else:
            monthly_percentage = '0'

        monthly_price = monthly_price.replace(",","")
        item["monthly_price"] = monthly_price
        item["monthly_percent"] = monthly_percentage

        ccd1 = soup.find('div',{'class':'ccd1'})
        coincontainerdiv = ccd1.find('div',{'class':'coincontainerdiv'})




        overview_container1 = container.find('div',{'class':'overviewcontainer1'})
        ccr2 = overview_container1.find('div',{'class':'ccr2'})
        coincrcol = ccr2.find('div',{'class':'coincrcol'})
        coin_bodies = coincrcol.findAll('div',{'class':'coinbodytext'})
        coin_type = coin_bodies[1].text
        release = coin_bodies[3].text
        website = coincrcol.find('img',{'alt':'Cryptocurrency Blockchain Website'})["src"]
        reddit = coincrcol.find('img',{'alt':'Cryptocurrency Blockchain Reddit'})["src"]
        twitter = coincrcol.find('img',{'alt':'Cryptocurrency Blockchain Twitter'})["src"]
        facebook = coincrcol.find('img',{'alt':'Cryptocurrency Blockchain Facebook'})["src"]

        coinsummary = ccr2.find('div',{'class':'coinsummary'}).text

        item["coin_type"] = coin_type
        item["website url"] = website
        item["release"] = release
        item["reddit"] = reddit
        item["twitter"] = twitter
        item["facebook"] = facebook
        item["coin summary"] = coinsummary

        print("coin type : "+coin_type)
        print("release : "+release)
        print("website : "+website)
        print("reddit : "+reddit)
        print("twitter : "+twitter)
        print("facebook : "+facebook)
        print("coinsummary : "+coinsummary)
        


        coin_sections = soup.findAll('div',{'class':'coinsection2'})
        performance_container = coin_sections[0]
        market_cap = ''
        volume24h = ''
        circsupply = ''
        maxsupply = ''
        released = ''
        try:
            market_cap = performance_container.find('div',{'class':'marketcap'}).text
        except Exception:
            pass
        try:
            volume24h = performance_container.find('div',{'class':'volume24h'}).text
        except Exception:
            pass
        try:
            circsupply = performance_container.find('div',{'class':'circsupply'}).text
        except Exception:
            pass
        try:
            maxsupply = performance_container.find('div',{'class':'maxsupply'}).text
        except Exception:
            pass
        try:
            released = performance_container.find('div',{'class':'released'}).text
        except Exception:
            pass

        item["market cap"] = market_cap
        item["24h volume"] = volume24h
        item["circulation supply"] = circsupply
        item["max supply"] = maxsupply
        item["Released"] = released

        print(market_cap)
        print(volume24h)
        print(circsupply)
        print(maxsupply)
        print(released)

        usecase_container = coin_sections[1]
        usecol = usecase_container.find('div',{'class':'usecol'})
        # iframe = usecol.find('iframe',{'class':'embedly-embed'})
        # youtube_link = iframe.find('iframe',{'id':'player'})['src']
        collections = usecase_container.findAll('div',{'class':'collectionstext'})
        collections_str = ''
        if collections:
            for collection in collections:
                collections_str += collection.text + "   "
            collections_str = collections_str.strip()
    
        social_score = ''
        reddit_subscribers = ''
        reddit_subscribers_pct = ''
        twitter_followers = ''
        twitter_followers_pct = ''
        facebook_likes = ''
        facebook_likes_pct = ''

        social_container = coin_sections[2]
        social_score = social_container.find('div',{'class':'social-score'}).text
        socialscorepercent = social_container.find('div',{'class':'socialscorepercent'}).text
        reddit_subscribers = social_container.find('div',{'class':'reddit-subscribers'}).text
        reddit_subscribers_pct = social_container.find('div',{'class':'reddit-subscribers-pct'}).text
        twitter_followers = social_container.find('div',{'class':'twitter-followers'}).text
        twitter_followers_pct = social_container.find('div',{'class':'twitter-followers-pct'}).text
        facebook_likes = social_container.find('div',{'class':'facebook-likes'}).text
        facebook_likes_pct = social_container.find('div',{'class':'facebook-likes-pct'}).text
#        print(youtube_link)
        item["use case collections"] = collections_str
        item["social score"] = social_score
        item["reddit sub"] = reddit_subscribers
        item["reddit pct"] = reddit_subscribers_pct
        item["twitter followers"] = twitter_followers
        item["twitter pct"] = twitter_followers_pct
        item["facebook likes"] = facebook_likes
        item["facebook pct"] = facebook_likes_pct

        print(social_score)
        print(socialscorepercent)
        print(reddit_subscribers)
        print(reddit_subscribers_pct)
        print(twitter_followers)
        print(twitter_followers_pct)
        print(facebook_likes)
        print(facebook_likes_pct)


        team_container = coin_sections[3]
        teams = team_container.findAll('div',{'class':'collectionitenmteam'})
        teams_str = ''
        for team in teams:
            developer = team.find('div',{'class':'text-block-14'}).text
            job = team.find('div',{'class':'tb14'}).text
            dev_str = developer + " ("+job+")"
            teams_str += dev_str +" ,"

        print(teams_str)

        ico_container = coin_sections[4]
        ico_texts = ico_container.findAll('div',{'class':'performancetext'})
        ico_amount_raised = ico_texts[0].text
        ico_bonus = ico_texts[1].text
        ico_min_invest = ico_texts[2].text
        ico_founding_target = ico_texts[3].text
        ico_founding_cap = ico_texts[4].text
        ico_country = ico_texts[5].text

        item["team"] = teams_str
        item["amount raised"] = ico_amount_raised
        item["ico bonus"] = ico_bonus
        item["min investment"] = ico_min_invest
        item["founding target"] = ico_founding_target

        item["founding cap"] = ico_founding_cap
        item["country"] = ico_country

        print(ico_amount_raised)
        print(ico_bonus)
        print(ico_min_invest)
        print(ico_founding_target)
        print(ico_founding_cap)
        print(ico_country)

        technology_container = coin_sections[5]
        prooftypetext = technology_container.findAll('div',{'class':'prooftypetext'})[1].text
        performancetexts = technology_container.findAll('div',{'class':'performancetext'})
        te_platform = performancetexts[1].text
        te_mobile_wallet = performancetexts[2].text
        te_web_wallet = performancetexts[3].text
        te_hardware_wallet = performancetexts[4].text

        item["proof type"] = prooftypetext
        item["platform"] = te_platform
        item["mobile wallet"] = te_mobile_wallet
        item["web wallet"] = te_web_wallet
        item["hardware wallet"] = te_hardware_wallet
        print(prooftypetext)
        print(te_platform)
        print(te_mobile_wallet)
        print(te_web_wallet)
        print(te_hardware_wallet)

        return item

def main():
    upfolio = Upfolio()
    upfolio.get_coins()
    upfolio.scrape_all()
    
if __name__ == "__main__":main()
