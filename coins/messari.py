import requests
from bs4 import BeautifulSoup
import json

class Messari:
    def __init__(self):
        with open("setting.json") as f:
            settings = json.load(f)
            self.db_name = settings["db name"]
            self.host = settings["host"]
            self.user = settings["user"]
            self.password = settings["password"]

            self.url = "https://data.messari.io/api/v1/markets/prices-legacy"
    def scrape(self):
        source = requests.get(self.url).text
        json_result = json.loads(source)
        json_data = json_result["data"]
        result = []
        for item in json_data:
            item_json = {}
            item_json["name"] = item["name"]
            item_json["symbol"] = item["symbol"]
            try:
                item_json["priceUsd"] = item["priceUsd"]
            except Exception:
                item_json["priceUsd"]= ''
            try:
                item_json["percentageChange24HrUsd"] = item["percentageChange24HrUsd"]
            except Exception:
                item_json["percentageChange24HrUsd"] = ''
            try:
                item_json["realVol24HrUsd"] = item["realVol24HrUsd"]
            except Exception:
                item_json["realVol24HrUsd"] = ''
            try:
                item_json["y2050Marketcap"] = item["y2050Marketcap"]
            except Exception:
                item_json["y2050Marketcap"] = ''
            try:
                item_json["liquidMarketcap"] = item["liquidMarketcap"]
            except Exception:
                item_json["liquidMarketcap"] = ''
            try:        
                item_json["supplyPercentageIssued"] = item["supplyPercentageIssued"]
            except Exception:
                item_json["supplyPercentageIssued"] = ''
            try:
                item_json["percentageDownFromAth"] = item["percentageDownFromAth"]
            except Exception:
                item_json["percentageDownFromAth"] = ''
            try:
                item_json["flipsideRating"] = item["flipsideRating"]
            except Exception:
                item_json["flipsideRating"] = ''
            try:
                item_json["percentageChangeYtdUsd"] = item["percentageChangeYtdUsd"]
            except Exception:
                item_json["percentageChangeYtdUsd"] = ''

            result.append(item_json)
        
        print(result)
    def start(self):
        self.scrape()

def main():
    messari = Messari()
    messari.start()
    
if __name__ == "__main__":main()

