import requests
from bs4 import BeautifulSoup
import json

class Messari:
    def __init__(self):
        with open("setting.json") as f:
            settings = json.load(f)
            self.db_name = settings["db name"]
            self.host = settings["host"]
            self.user = settings["user"]
            self.password = settings["password"]

            self.url = "https://coincheckup.com/data/prod/201903311241/coins.json"
    def scrape(self):
        source = requests.get(self.url).text
        json_data = json.loads(source)
        result = []
        for item in json_data:
            item_json = {}
            item_json["name"] = item["name"]
            item_json["symbol"] = item["symbol"]
            item_json["score"] = item["score"]
            print(item_json["name"])


        #     result.append(item_json)
        
        # print(result)
    def start(self):
        self.scrape()

def main():
    messari = Messari()
    messari.start()
    
if __name__ == "__main__":main()

