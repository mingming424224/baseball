import requests
from bs4 import BeautifulSoup
import json
import time
from datetime import datetime

class Bovada:
    def __init__(self):
        pass
    def scrape(self):
        headers = {'scheme':'https','user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36'}
        urls = {
            'https://www.bovada.lv/services/sports/event/v2/events/A/description/baseball/mlb?marketFilterId=def&liveOnly=true&lang=en',
            'https://www.bovada.lv/services/sports/event/v2/events/A/description/baseball/mlb?marketFilterId=705&preMatchOnly=true&lang=en',
            'https://www.bovada.lv/services/sports/event/v2/events/A/description/baseball/mlb?marketFilterId=706&preMatchOnly=true&lang=en',
            'https://www.bovada.lv/services/sports/event/v2/events/A/description/baseball/mlb?marketFilterId=301&preMatchOnly=true&lang=en',
            'https://www.bovada.lv/services/sports/event/v2/events/A/description/baseball/mlb?marketFilterId=302&preMatchOnly=true&lang=en',
            'https://www.bovada.lv/services/sports/event/v2/events/A/description/baseball/mlb?marketFilterId=team-totals&preMatchOnly=true&lang=en',
            }
        batch_rows = []

        for url in urls:
            source = requests.get(url,headers=headers).text
            json_data = json.loads(source) 
            if len(json_data) == 0:
                continue
            for event in json_data[0]["events"]:
                title = event["description"]
                start_time = event["startTime"]
                competitors = event["competitors"]
                first_team = competitors[1]["name"]
                second_team = competitors[0]["name"]
                groups = event["displayGroups"]
                bet = groups[0]["description"]
                markets = groups[0]["markets"]
                first_runline = ''
                try:
                    first_runline = markets[0]["outcomes"][0]["price"]["handicap"] + "("+ markets[0]["outcomes"][0]["price"]["handicap"] + ")"
                except Exception:
                    pass
                second_runline = ''
                try:    
                    second_runline = markets[0]["outcomes"][1]["price"]["handicap"] + "("+ markets[0]["outcomes"][1]["price"]["handicap"] + ")"
                except Exception:
                    pass
                first_ml = ''
                second_ml = ''
                try:
                    first_ml = markets[1]["outcomes"][0]["price"]["american"]
                except Exception:
                    pass
                try:
                    second_ml = markets[1]["outcomes"][1]["price"]["american"]
                except Exception:
                    pass

                first_total = ''
                second_total = ''
                try:
                    first_total = markets[2]["outcomes"][0]["price"]["handicap"] + "(" + markets[2]["outcomes"][0]["price"]["american"] + ")"
                except Exception:
                    pass
                try: 
                    second_total = markets[2]["outcomes"][1]["price"]["handicap"] + "(" + markets[2]["outcomes"][1]["price"]["american"] + ")"
                except Exception:
                    pass  
                time1 = datetime.fromtimestamp(int(start_time)/1000)
                match_date = str(time1.today().year) +"-"+ str(time1.today().month) +"-"+ str(time1.today().day) 
                match_time = str(time1.now().hour) +":"+str(time1.now().minute) + ":" + str(time1.now().second) 
                match_json = []
                match_json.append(match_date)
                match_json.append(match_time)
                match_json.append(title)
                match_json.append(first_team)
                match_json.append(bet)
                match_json.append(first_runline)
                match_json.append(first_ml)
                match_json.append(first_total)
                match_json.append("Bovada")
                batch_rows.append(match_json)
                print(match_json)

                match_json = []
                match_json.append(match_date)
                match_json.append(match_time)
                match_json.append(title)
                match_json.append(second_team)
                match_json.append(bet)
                match_json.append(second_runline)
                match_json.append(second_ml)
                match_json.append(second_total)
                match_json.append("Bovada")
                batch_rows.append(match_json)
                print(match_json)

        return batch_rows


def main():
    bovada = Bovada()
    bovada.scrape()
    
if __name__ == "__main__":main()




