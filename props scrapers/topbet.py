import requests
from bs4 import BeautifulSoup
import json


class Topbet:
    def __init__(self):
        pass
    def scrape(self):
        url = "https://topbet.eu/sportsbook/baseball/mlbearly-action-props"
        source = requests.get(url).text
        soup = BeautifulSoup(source, 'lxml')
        container = soup.find('div',{'class':'table-container'})
        games = container.findAll('div',{'class':'gamelines-event'})
        batch_rows = []
        for game in games:
            sub_event = game.find('div',{'class':'gamelines-subevent'})
            title = sub_event.find('div',{'class':'gamelines-event-title'}).text
            date = sub_event.find('div',{'class':'gamelines-event-time'}).text
            details = game.find('div',{'class':'gamelines-details'})
            time_container = details.find('div',{'class':'time'})
            time1 = time_container.find('div',{'class':'countdown'}).text

            teams = details.findAll('div',{'class':'gamelines-team'})
            first_team = teams[0].find('div',{'class':'team-title'}).text
            first_spread = teams[0].find('a',{'class':'btn-spread'}).text

            push_row = []
            push_row.append(date.strip())
            push_row.append(time1.strip())
            push_row.append(title.strip())
            push_row.append(first_team.strip())
            push_row.append("MLB-Early Action Props Game Lines")
            push_row.append("")
            push_row.append("")
            push_row.append("")
            push_row.append(first_spread.strip())
            push_row.append("TopBet.eu")
            batch_rows.append(push_row)
            print(push_row)
            
            second_team = teams[1].find('div',{'class':'team-title'}).text
            second_spread = teams[1].find('a',{'class':'btn-spread'}).text

            push_row = []
            push_row.append(date.strip())
            push_row.append(time1.strip())
            push_row.append(title.strip())
            push_row.append(second_team.strip())
            push_row.append("MLB-Early Action Props Game Lines")
            push_row.append("")
            push_row.append("")
            push_row.append("")
            push_row.append(second_spread.strip())
            push_row.append("TopBet.eu")
            batch_rows.append(push_row)
            print(push_row)

        return batch_rows


def main():
    topbet = Topbet()
    topbet.scrape()
    
if __name__ == "__main__":main()




