from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import requests
from bs4 import BeautifulSoup
import json
import time
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options 

from abcislands import Abcislands
from betmania import Betmania
from betonline import BetOnline
from betphoenix import Betphoenix
from betpop import Betpop
from bovada import Bovada
from mybookie import Mybookie
from sports724 import Sports724
from sportsbetting import Sportsbetting
from topbet import Topbet
from xbet import Xbet
from youwanger import Youwager

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

sheet_id = "1bZDdENMucVbe8ypDEFLzS2rnQtCuIoL6vZ2m8_UOTqg"

SAMPLE_RANGE_NAME = 'MLB Props!A2:J'

class PropsScraper():
    def __init__(self):
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.


        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        service = build('sheets', 'v4', credentials=creds)
        self.sheet = service.spreadsheets()
    def collect_values(self):
        value_list = []

        abcislands = Abcislands()
        result = abcislands.scrape()
        value_list = value_list + result

        betmania = Betmania()
        result = betmania.scrape()
        value_list = value_list + result

        betOnline = BetOnline()
        result = betOnline.scrape()
        value_list = value_list + result

        betphoenix = Betphoenix()
        result = betphoenix.scrape()
        value_list = value_list + result

        betpop = Betpop()
        result = betpop.scrape()
        value_list = value_list + result

        bovada = Bovada()
        result = bovada.scrape()
        value_list = value_list + result

        mybookie = Mybookie()
        result = mybookie.scrape()
        value_list = value_list + result

        sports724 = Sports724()
        result = sports724.scrape()
        value_list = value_list + result
        
        sportsbetting = Sportsbetting()
        result = sportsbetting.scrape()
        value_list = value_list + result

        topbet = Topbet()
        result = topbet.scrape()
        value_list = value_list + result

        xbet = Xbet()
        result = xbet.scrape()
        value_list = value_list + result

        youwager = Youwager()
        result = youwager.scrape()
        value_list = value_list + result

        return value_list

    def start_scrape(self):
        while True:
            values = self.collect_values()
            body = {
                'values':values
            }
            self.sheet.values().clear(spreadsheetId=sheet_id, range=SAMPLE_RANGE_NAME, body={}).execute()
            self.sheet.values().append(spreadsheetId=sheet_id,range=SAMPLE_RANGE_NAME,valueInputOption="USER_ENTERED", body=body).execute()
            time.sleep(300)

def main():
    propsScraper = PropsScraper()
    propsScraper.start_scrape()
    
if __name__ == "__main__":main()
    

