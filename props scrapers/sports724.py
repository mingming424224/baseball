import requests
from bs4 import BeautifulSoup
import json

class Sports724:
    def __init__(self):
        pass
    def get_data(self):
        url = "http://www.724sports.com/LiveLines.aspx"

        source = requests.get(url).text
        soup = BeautifulSoup(source, 'lxml')
        menu = soup.select('#menuLiveLines > li:nth-child(10) > ul')
        lies = menu[0].findAll('li')

        data_json = {}
        data_list = []
        game_json = {}
        game_list = []

        for li in lies:
            a_link = None
            a_link = li.find('a')
            if a_link:
                sub_url = "http://www.724sports.com/" + a_link['href'] 
                prop_source = requests.get(sub_url).text
                prop_soup = BeautifulSoup(prop_source, 'lxml')

                contentHP4 = prop_soup.find('td',{'id':'contentHP4'})
                tbody = contentHP4.find('table')
                trs = tbody.findAll('tr')
                start_pos = 0
                end_pos = 0
                flag = True
                for i in range(0,len(trs)):
                    if flag == True:
                        tr_class = ''
                        try:
                            tr_class = trs[i]['class'][0]
                        except Exception:
                            pass

                        if tr_class == 'TrGameOddLL':
                            start_pos = i
                            flag = False

                flag = True
                for i in range(start_pos + 1,len(trs)):
                    if flag == True:
                        tr_class = ''
                        try:
                            tr_class = trs[i]['class'][0]
                        except Exception:
                            pass
                        if tr_class == '':
                            end_pos = i
                            flag = False
                game_json = {}
                title = li.text
                title = title.strip()
                game_list = []
                for i in range(start_pos,end_pos,2):
                    tds1 = trs[i].findAll('td')
                    if len(tds1) < 5:
                        continue
                    tds2 = trs[i+1].findAll('td')
                    cur_time1 = tds1[1].text
                    cur_time2 = tds2[1].text
                    first_rot = tds1[2].text
                    first_team = tds1[3].text
                    first_spread = tds1[6].text
                    first_ml = tds1[4].text
                    first_total = tds1[5].text

                    first_spread = first_spread.strip()
                    if first_spread[0:1] == "+":
                        first_spread = first_spread[1:]

                    second_rot = tds1[2].text
                    second_team = tds1[3].text
                    second_spread = tds1[6].text
                    second_ml = tds1[4].text
                    second_total = tds1[5].text

                    second_spread = second_spread.strip()
                    if second_spread[0:1] == "+":
                        second_spread = second_spread[1:]



                    match_json = {}
                    match_json["time1"] = cur_time1.strip()
                    match_json["time2"] = cur_time2.strip()

                    match_json["first rot"] = first_rot.strip()
                    match_json["first team"] = first_team.strip()
                    match_json["first spread"] = first_spread
                    match_json["first ml"] = first_ml.strip()
                    match_json["first total"] = first_total.strip()

                    match_json["second rot"] = second_rot.strip()
                    match_json["second team"] = second_team.strip()
                    match_json["second spread"] = second_spread
                    match_json["second ml"] = second_ml.strip()
                    match_json["second total"] = second_total.strip()
                    
                    match_json["Bet"] = title
                    game_list.append(match_json)

                game_json["data"] = game_list
                data_list.append(game_json)

        data_json["games"] = data_list

        return data_json
    
    def scrape(self):
        result_json = self.get_data()
        batch_rows = []
        for game in result_json["games"]:
            for match in game["data"]:
                push_row = []
                push_row.append(match["time1"])
                push_row.append(match["time2"])
                push_row.append("")
                push_row.append(match["first team"])
                push_row.append(match["Bet"])
                push_row.append(match["first spread"])
                push_row.append(match["first ml"])
                push_row.append(match["first total"])
                push_row.append("")
                push_row.append("724 Sports")
                batch_rows.append(push_row)
                print(push_row)

                push_row = []
                push_row.append(match["time1"])
                push_row.append(match["time2"])
                push_row.append("")
                push_row.append(match["second team"])
                push_row.append(match["Bet"])
                push_row.append(match["second spread"])
                push_row.append(match["second ml"])
                push_row.append(match["second total"])
                push_row.append("")
                push_row.append("724 Sports")
                batch_rows.append(push_row)
                print(push_row)
        return batch_rows


def main():
    sports724 = Sports724()
    sports724.scrape()
    
if __name__ == "__main__":main()
