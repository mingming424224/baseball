import requests
from bs4 import BeautifulSoup
import json
import re

class Betphoenix:
    def __init__(self):
        pass

    def get_fraction(self,frac_str):
        result =  re.search('&frac(.*);', frac_str)
        spread1 = frac_str
        if result:
            spread = result.group(1)
            spread1 =  frac_str.replace("&frac"+result.group(1)+";"," "+spread[0]+"/" + spread[1])
        return spread1
    def scrape(self):
        url1 = "https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112006797045150796266_1560445884377&hid=bdb01c9f736f2e641984fcde2fce11ad&cmd=games&leagues=111&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560445884408"
        url2 = "https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112006797045150796266_1560445884377&hid=bdb01c9f736f2e641984fcde2fce11ad&cmd=games&leagues=97&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560445884409"
        url3 = "https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112006797045150796266_1560445884377&hid=bdb01c9f736f2e641984fcde2fce11ad&cmd=games&leagues=284&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560445884410"
        url4 = "https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112006797045150796266_1560445884377&hid=bdb01c9f736f2e641984fcde2fce11ad&cmd=games&leagues=285&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560445884412"
        urls = [{'bet':'MLB ALTERNATIVE RUNLINES','url':url1},{'bet':'MLB TEAM TO SCORE FIRST IN GAME','url':url2},{'bet':'MLB WILL THERE BE A SCORE IN THE 1ST INNING','url':url3},{'bet':'MLB TOTAL HITS+RUNS+ERRORS IN THE GAME','url':url4}]
        headers = {'scheme':'https','user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36'}
        
        batch_rows = []
        for url in urls:

            source = requests.get(url['url'],headers=headers).text
            pos1 = source.find("(")
            source = source[pos1+1:len(source)-2]
            json_data = json.loads(source)

            data_list = []
            game_json = {}
            game_list = []

            flag = False
            title = ''
            for game in json_data["games"]:
                lines = game["lines"]
                ab = True
                try:
                    ab_f = game["ab"]
                except Exception:
                    ab = False
                    pass
                if ab == True:
                    if flag == True:
                        game_json["data"] = game_list
                        data_list.append(game_json)
                    else:
                        flag = True
                    title = game["htm"]
                    if title == '':
                        title = game["vtm"]
                    continue
                elif len(lines) > 0:
                    line = lines[0]
                    first_team = game["vtm"] + " " +game["vpt"]
                    second_team = game["htm"] + " " + game["hpt"]
                    first_ml = line["voddsh"]
                    second_ml = line["hoddsh"]
                    first_total = line["ovh"]
                    second_total = line["unh"]
                    first_spread = line["vsprdh"]
                    second_spread = line["hsprdh"]
                    first_spread = first_spread.strip()
                    first_spread = self.get_fraction(first_spread)
                    if first_spread[0:1] == "+":
                        first_spread = first_spread[1:]

                    first_total = first_total.strip()
                    first_total = self.get_fraction(first_total)

                    second_total = second_total.strip()
                    second_total = self.get_fraction(second_total)

                    second_spread = second_spread.strip()
                    second_spread = self.get_fraction(second_spread)
                    if second_spread[0:1] == "+":
                        second_spread = second_spread[1:]

                    match_json = []
                    match_json.append(game["gmdt"])
                    match_json.append(game["gmtm"])
                    match_json.append(title.strip())
                    match_json.append(first_team.strip())
                    match_json.append(url['bet'])
                    match_json.append(first_spread)
                    match_json.append(first_ml.strip())
                    match_json.append(first_total)
                    match_json.append("")
                    match_json.append("Bet Phoenix")
                    batch_rows.append(match_json)
                    print(match_json)

                    match_json = []
                    match_json.append(game["gmdt"])
                    match_json.append(game["gmtm"])
                    match_json.append(title.strip())
                    match_json.append(second_team.strip())
                    match_json.append(url['bet'])
                    match_json.append(second_spread)
                    match_json.append(second_ml.strip())
                    match_json.append(second_total)
                    match_json.append("")
                    match_json.append("Bet Phoenix")
                    batch_rows.append(match_json)
                    print(match_json)
        return batch_rows

def main():
    betphoenix = Betphoenix()
    betphoenix.scrape()
    
if __name__ == "__main__":main()
