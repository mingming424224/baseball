import requests
from bs4 import BeautifulSoup
import json
import re

class Abcislands:
    def __init__(self):
        pass

    def get_fraction(self,frac_str):
        result =  re.search('&frac(.*);', frac_str)
        spread1 = frac_str
        if result:
            spread = result.group(1)
            spread1 =  frac_str.replace("&frac"+result.group(1)+";"," "+spread[0]+"/" + spread[1])
        return spread1

    def get_data(self):
        headers = {'content-type':'application/json'}
        data  = {"IdPlayer": 0,"LineStyle": "E","LeagueList":[140]}
        
        source = requests.post("https://betslipapi.isppro.net/api/Guest/GetNewLines",json=data,headers=headers).text
        json_data = json.loads(source)
        if len(json_data) == 0:
            print("no data")
            return []
        games = json_data[0]['Games']

        data_json = {}
        data_list = []
        game_json = {}
        game_list = []
        game_json["title"] = json_data[0]['Description']

        for game in games:
            match_json={}
            gmdt = game['Gmdt']
            match_json["time1"] = gmdt[0:4] +"-"+gmdt[4:6]+"-"+gmdt[6:]
            match_json["time2"] = game['Gmtm']
            line = game['Lines'][0]
            match_json["first rot"] = game['Vnum']
            match_json["second rot"] = game['Hnum']
            match_json["first team"] = game['Vtm'] + " " + game["Vpt"]
            match_json["second team"] = game['Htm'] + " " + game["Hpt"]
            match_json["game"] = game['Gdesc']
            if line['Vsprdh'].strip() != '':
                match_json["first spread"] =self.get_fraction(line['Vsprdh'].strip())
            else:
                match_json["first spread"] = ''

            if line['Hsprdh'].strip() != '':
                match_json["second spread"] = self.get_fraction(line['Hsprdh'].strip())
            else:
                match_json["second spread"] = ''

            match_json["first ml"] = line['Voddsh']
            match_json["second ml"] = line['Hoddsh']
            if line['Ovh'].strip() != '':
                match_json["first total"] = self.get_fraction(line['Ovh'].strip())
            else:
                match_json["first total"] = ''
            if line['Unh'].strip() != '':
                match_json["second total"] = self.get_fraction(line['Unh'].strip())
            else:
                match_json["second total"] = ''

            game_list.append(match_json)
        game_json["data"] = game_list
        data_list.append(game_json)
        data_json["games"] = data_list

        return data_json

    def scrape(self):
        result_json = self.get_data()
        batch_rows = []
        if len(result_json) > 0:
            for match in result_json["games"][0]["data"]:
                push_row = []
                push_row.append(match["time1"])
                push_row.append(match["time2"])
                push_row.append(match["game"])
                push_row.append(match["first team"])
                push_row.append("GAME PROPS")
                push_row.append(match["first spread"])
                push_row.append(match["first ml"])
                push_row.append(match["first total"])
                push_row.append("")
                push_row.append("ABC Islands")
                batch_rows.append(push_row)
                print(push_row)

                push_row = []
                push_row.append(match["time1"])
                push_row.append(match["time2"])
                push_row.append(match["game"])
                push_row.append(match["second team"])
                push_row.append("GAME PROPS")
                push_row.append(match["second spread"])
                push_row.append(match["second ml"])
                push_row.append(match["second total"])
                push_row.append("")
                push_row.append("ABC Islands")
                batch_rows.append(push_row)
                print(push_row)

        return batch_rows
    
def main():
    abcislands = Abcislands()
    abcislands.scrape()
    
if __name__ == "__main__":main()
