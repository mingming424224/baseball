import requests
from bs4 import BeautifulSoup
import json

class Xbet:
    def __init__(self):
        pass

    def scrape(self):
        url = "https://xbet.ag/sportsbook/mlb/game-props/"
        source = requests.get(url).text
        soup = BeautifulSoup(source, 'lxml')
        content = soup.find('div',{'class':'sportsbook__content-bg'})
        batch_rows = []
        if content:
            games = content.findAll('div',{'class':['sportsbook__line-body-box','sportsbook__header-line']})
            bet_name = '' 
            for game in games:
                body_head = game.find('div',{'class':'sportsbook__line-body-head'})
                if body_head:
                    time_container = game.find('p',{'class':'sportsbook__line-date'})
                    time1 = time_container.text
                    date = time_container["data-time"]


                    body_content = game.find('div',{'class':'sportsbook__line-content'})

                    visitor = body_content.find('div',{'class':'sportsbook__line-team-visitor'})
                    first_team = visitor.find('p',{'class':'sportsbook__line-team-name'}).text
                    odds1 = visitor.findAll('button',{'class':'lines-odds'})
                    first_total = odds1[1].text
                    first_ml = odds1[2].text

                    local = body_content.find('div',{'class':'sportsbook__line-team-local'})
                    second_team = local.find('p',{'class':'sportsbook__line-team-name'}).text
                    odds2 = local.findAll('button',{'class':'lines-odds'})
                    second_total = odds2[1].text
                    second_ml = odds2[2].text
                    
                    push_row = []
                    push_row.append(date.strip())
                    push_row.append(time1.strip())
                    push_row.append("")
                    push_row.append(first_team.strip())
                    push_row.append(bet_name.strip())
                    push_row.append("")
                    push_row.append(first_ml.strip())
                    push_row.append(first_total.strip())
                    push_row.append("")
                    push_row.append("Xbet")
                    batch_rows.append(push_row)
                    print(push_row)

                    push_row = []
                    push_row.append(date.strip())
                    push_row.append(time1.strip())
                    push_row.append("")
                    push_row.append(second_team.strip())
                    push_row.append(bet_name.strip())
                    push_row.append("")
                    push_row.append(second_ml.strip())
                    push_row.append(second_total.strip())
                    push_row.append("")
                    push_row.append("Xbet")
                    batch_rows.append(push_row)
                    print(push_row)

                else:
                    class_len = len(game['class'])
                    if class_len < 3:
                        bet_name = game.find('small').text
        return batch_rows

def main():
    xbet = Xbet()
    xbet.scrape()
    
if __name__ == "__main__":main()


