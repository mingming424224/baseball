from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from bs4 import BeautifulSoup
import json
import time
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options 

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

# The ID and range of a sample spreadsheet.
SAMPLE_RANGE_NAME1 = 'Signal Profits (Swing Signals)!A2:K'
SAMPLE_RANGE_NAME2 = 'Signal Profits Updates!A2:C'
SAMPLE_RANGE_NAME3 = 'Signal Profits Content!A2:C'

class Signal():
    def __init__(self):
        with open("setting.json") as f:
            settings = json.load(f)
            self.spreadsheet_id = settings["sheet id"]
            self.user_email = settings["user email"]
            self.password = settings["user password"]
            self.executable_path = settings["chrome executable path"]
            self.profile_path = settings["chrome profile path"]

        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.


        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        service = build('sheets', 'v4', credentials=creds)
        self.sheet = service.spreadsheets()

        chrome_options = Options()
        chrome_options.binary_location = self.executable_path
        chrome_options.add_argument("user-data-dir="+self.profile_path)
    
#        chrome_options.add_argument("--headless") 
#        prefs = {'profile.managed_default_content_settings.images':2}
#        chrome_options.add_experimental_option("prefs", prefs)
#        chrome_path = r"chromedriver.exe" 
        self.driver = webdriver.Chrome(chrome_options=chrome_options)

    def login(self):
        self.driver.get("https://app.signalprofits.com/app/login")
        try:
            self.driver.find_element_by_xpath("""//*[@id="pagey"]/div[1]/div/form/div[1]/input""").send_keys(self.user_email)
            self.driver.find_element_by_xpath("""//*[@id="pagey"]/div[1]/div/form/div[2]/input""").send_keys(self.password)
            self.driver.find_element_by_xpath("""//*[@id="pagey"]/div[1]/div/form/button""").click()
        except Exception:
            pass

    def scrape(self):
        try:
            element_present = EC.presence_of_element_located((By.CLASS_NAME,"coin-signals"))
            WebDriverWait(self.driver, 300).until(element_present)
            time.sleep(1)
        except Exception:
            print("timeout!")
        soup = BeautifulSoup(self.driver.page_source, 'lxml')
        signal_container = soup.find('div',{'class':'coin-signals'})
        signals = signal_container.findAll('a',{'class':'coin'})
        self.sheet.values().clear(spreadsheetId=self.spreadsheet_id, range=SAMPLE_RANGE_NAME1, body={}).execute()
        self.sheet.values().clear(spreadsheetId=self.spreadsheet_id, range=SAMPLE_RANGE_NAME2, body={}).execute()
        self.sheet.values().clear(spreadsheetId=self.spreadsheet_id, range=SAMPLE_RANGE_NAME3, body={}).execute()
        values = []
        update_values = []
        for signal in signals:
            signal_id = signal["post-id"]
            signal_url = "https://app.signalprofits.com/wp-admin/admin-ajax.php?action=get_signal&ID=" + signal_id
            self.driver.get(signal_url)
            sub_soup = BeautifulSoup(self.driver.page_source, 'lxml')
            sub_source = sub_soup.find('pre').text
            json_data = json.loads(sub_source)
            created_date = json_data["date"]
            trading_pair = json_data["symbol_pair"]
            time_frame = json_data["timeframeStr"]
            risk = json_data["risk"]
            entry = json_data["entry"]
            targets = json_data["targets"]
            target1 = ''
            target2 = ''
            target3 = ''
            target4 = ''
            try:
                target1 = targets[0]["target"] + " (" + targets[0]["percent"] + ")"
            except Exception:
                pass
            try:
                target2 = targets[1]["target"] + " (" + targets[1]["percent"] + ")"
            except Exception:
                pass
            try:
                target3 = targets[2]["target"] + " (" + targets[2]["percent"] + ")"
            except Exception:
                pass
            try:
                target4 = targets[3]["target"] + " (" + targets[3]["percent"] + ")"
            except Exception:
                pass
            stop = str(json_data["stop_losses"][0]["price"]) + " (" + str(json_data["stop_losses"][0]["percent"]) + "%)"
            content = json_data["content"]
            con_soup = BeautifulSoup(content, 'lxml')
            notes = con_soup.text
            update_url = "https://app.signalprofits.com/wp-admin/admin-ajax.php?action=get_signal_updates&limit=2&offset=0&coin="+str(json_data["coin"]["id"])+"&category=&search=&product=signals"
            self.driver.get(update_url)
            sub_soup = BeautifulSoup(self.driver.page_source, 'lxml')
            sub_source = sub_soup.find('pre').text
            json_updates = json.loads(sub_source)

            for update in json_updates:
                update_soup = BeautifulSoup(update["content"], 'lxml')
                update_title = update["title"]
                update_date = update["date"]
                item = []
                item.append(update_date)
                item.append(update_title)
                item.append(update_soup.text)
                update_values.append(item)
            item = []
            item.append(created_date)
            item.append(trading_pair)
            item.append(time_frame)
            item.append(risk)
            item.append(entry)
            item.append(target1)
            item.append(target2)
            item.append(target3)
            item.append(target4)
            item.append(stop)
            item.append(notes)
            print(item)
            values.append(item)
        body1 = {
            'values':values
        }

        self.sheet.values().append(spreadsheetId=self.spreadsheet_id,range=SAMPLE_RANGE_NAME1,valueInputOption="USER_ENTERED", body=body1).execute()

        body2 = {
            'values':update_values
        }
        self.sheet.values().append(spreadsheetId=self.spreadsheet_id,range=SAMPLE_RANGE_NAME2,valueInputOption="USER_ENTERED", body=body2).execute()


        values = []
        update_container = soup.find('div',{'id':'all-updates'})
        updates = update_container.findAll('div',{'class':'update-item'})
        for update in updates[2:]:
            u_date = update.find('div',{'class':'date'}).text
            title = update.find('div',{'class':'title'}).text
            u_content = update.find('div',{'class':'content'}).text
            item = []
            item.append(u_date)
            item.append(title)
            item.append(u_content)
            values.append(item)
            print(item)
        body3 = {
            'values':values
        }

        self.sheet.values().append(spreadsheetId=self.spreadsheet_id,range=SAMPLE_RANGE_NAME3,valueInputOption="USER_ENTERED", body=body3).execute()
        
    def start(self):
        self.login()
        while True:
            self.driver.get("https://app.signalprofits.com/app/trading/")
            self.scrape()
            time.sleep(600)


def main():
    signal = Signal()
    signal.start()
    
if __name__ == "__main__":main()
