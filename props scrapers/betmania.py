import requests
from bs4 import BeautifulSoup
import json
import re

class Betmania:
    def __init__(self):
        pass

    def get_fraction(self,frac_str):
        result =  re.search('&frac(.*);', frac_str)
        spread1 = frac_str
        if result:
            spread = result.group(1)
            spread1 =  frac_str.replace("&frac"+result.group(1)+";"," "+spread[0]+"/" + spread[1])
        return spread1
    def scrape(self):
        urls = [
            {'bet':'MLB TEAM TOTALS','url':'https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112024191543520208625_1560539735451&hid=39c7f91c104319aa89968481b60d43ff&cmd=games&leagues=90&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560539735492'},
            {'bet':'MLB ALTERNATIVE RUNLINES','url':'https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112024191543520208625_1560539735451&hid=39c7f91c104319aa89968481b60d43ff&cmd=games&leagues=111&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560539735493'},
            {'bet':'MLB TEAM TO SCORE FIRST IN GAME','url':'https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112024191543520208625_1560539735451&hid=39c7f91c104319aa89968481b60d43ff&cmd=games&leagues=97&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560539735494'},
            {'bet':'MLB WILL THERE BE A SCORE IN THE 1ST INNING','url':'https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112024191543520208625_1560539735451&hid=39c7f91c104319aa89968481b60d43ff&cmd=games&leagues=284&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560539735496'},
            {'bet':'MLB TOTAL HITS+RUNS+ERRORS IN THE GAME','url':'https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112024191543520208625_1560539735451&hid=39c7f91c104319aa89968481b60d43ff&cmd=games&leagues=285&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560539735497'}
        ]

        batch_rows = []
        for url in urls:
            source = requests.get(url['url']).text
            pos1 = source.find("(")
            source = source[pos1+1:len(source)-2]
            json_data = json.loads(source)

            for game in json_data["games"]:
                lines = game["lines"]
                if len(lines) > 0:
                    line = lines[0]
                    first_team = game["vtm"] +" "+ game["vpt"]

                    second_team = game["htm"] + " "+ game["hpt"]
                    first_ml = line["voddsh"]
                    second_ml = line["hoddsh"]
                    first_total = line["ovh"]
                    second_total = line["unh"]
                    first_spread = line["vsprdh"]
                    second_spread = line["hsprdh"]

                    first_spread = first_spread.strip()
                    first_spread = self.get_fraction(first_spread)
                    if first_spread[0:1] == "+":
                        first_spread = first_spread[1:]

                    first_total = first_total.strip()
                    first_total = self.get_fraction(first_total)
                    if first_total[0:1] == "+":
                        first_total = first_total[1:]

                    second_spread = second_spread.strip()
                    second_spread = self.get_fraction(second_spread)
                    if second_spread[0:1] == "+":
                        second_spread = second_spread[1:]

                    second_total = second_total.strip()
                    second_total = self.get_fraction(second_total)
                    if second_total[0:1] == "+":
                        second_total = second_total[1:]

                    item = []
                    item.append(game["gmdt"])
                    item.append(game["gmtm"])
                    item.append("")
                    item.append(first_team.strip())
                    item.append(url['bet'])
                    item.append(self.get_fraction(first_spread.strip()))
                    item.append(first_ml.strip())
                    item.append(self.get_fraction(first_total.strip()))
                    item.append("")
                    item.append("Bet Mania")
                    batch_rows.append(item)
                    print(item)

                    item = []
                    item.append(game["gmdt"])
                    item.append(game["gmtm"])
                    item.append("")
                    item.append(second_team.strip())
                    item.append(url['bet'])
                    item.append(self.get_fraction(second_spread.strip()))
                    item.append(second_ml.strip())
                    item.append(self.get_fraction(second_total.strip()))
                    item.append("")
                    item.append("Bet Mania")
                    batch_rows.append(item)
                    print(item)
        return batch_rows


    
def main():
    betmania = Betmania()
    betmania.scrape()
    
if __name__ == "__main__":main()





