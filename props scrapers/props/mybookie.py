import requests
from bs4 import BeautifulSoup
import json
from datetime import datetime
from airtable import Airtable

class Mybookie:
    def __init__(self):
        with open("setting.json") as f:
            settings = json.load(f)
            api_key = settings["api key"]
            base_key = settings["mlb data"]
        self.airtable = Airtable(base_key, "MyBookie Props", api_key=api_key)

    def clear_airtable(self):
        pages = self.airtable.get_all()
        for page in pages:
            self.airtable.delete(page["id"])
    
    def scrape(self):
        url = "https://mybookie.ag/sportsbook/mlb/game-props/"
        source = requests.get(url).text
        soup = BeautifulSoup(source, 'lxml')
        content = soup.find('div',{'class':'sportsbook__content-bg'})
        games = content.findAll('div',{'class':['sportsbook__line-body-box','sportsbook__header-line']})
        batch_rows = []
        bet_name = '' 
        for game in games:
            body_head = game.find('div',{'class':'sportsbook__line-body-head'})
            if body_head:
                time_container = body_head.find('p',{'class':'moment_time_zone'})
                time1 = time_container.text
                date = time_container["data-time"]
                lines = game.findAll('p',{'class':'sportsbook__line-date'})
                title = lines[1].text


                body_content = game.find('div',{'class':'sportsbook__line-content'})

                visitor = body_content.find('div',{'class':'sportsbook__line-team-visitor'})
                first_rot = visitor.find('p',{'class':'sportsbook__line-rot'}).text
                first_team = visitor.find('p',{'class':'sportsbook__line-team-name'}).text
                odds1 = visitor.findAll('button',{'class':'lines-odds'})
                first_spread = odds1[0].text
                first_total = odds1[1].text
                first_ml = odds1[2].text

                local = body_content.find('div',{'class':'sportsbook__line-team-local'})
                second_rot = local.find('p',{'class':'sportsbook__line-rot'}).text
                second_team = local.find('p',{'class':'sportsbook__line-team-name'}).text
                odds2 = local.findAll('button',{'class':'lines-odds'})
                second_spread = odds2[0].text
                second_total = odds2[1].text
                second_ml = odds2[2].text

                push_row = {}
                push_row["Date"] = date.strip()
                push_row["Time"] = time1.strip()
                push_row["Game"] = title.strip()
                push_row["Team"] = first_team.strip()
                push_row["Bet"] = bet_name.strip()
                push_row["Total"] = first_total.strip()
                push_row["Money Line"] = first_ml.strip()
                push_row["Website"] = "My Bookie"
                batch_rows.append(push_row)
                print(push_row)

                push_row = {}
                push_row["Date"] = date.strip()
                push_row["Time"] = time1.strip()
                push_row["Game"] = title.strip()
                push_row["Team"] = second_team.strip()
                push_row["Bet"] = bet_name.strip()
                push_row["Total"] = second_total.strip()
                push_row["Money Line"] = second_ml.strip()
                push_row["Website"] = "My Bookie"
                batch_rows.append(push_row)
                print(push_row)


            else:
                class_len = len(game['class'])
                if class_len < 3:
                    bet_name = game.find('small').text
        self.airtable.batch_insert(batch_rows)

def main():
    mybookie = Mybookie()
    mybookie.clear_airtable()
    mybookie.scrape()
    
if __name__ == "__main__":main()


