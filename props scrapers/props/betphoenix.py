import requests
from bs4 import BeautifulSoup
import json
from datetime import datetime
from airtable import Airtable

class Betphoenix:
    def __init__(self):
        with open("setting.json") as f:
            settings = json.load(f)
            api_key = settings["api key"]
            base_key = settings["mlb data"]
        self.airtable = Airtable(base_key, "Bet Phoenix Props", api_key=api_key)
    def clear_airtable(self):
        pages = self.airtable.get_all()
        for page in pages:
            self.airtable.delete(page["id"])
    def scrape(self):
        url1 = "https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112006797045150796266_1560445884377&hid=bdb01c9f736f2e641984fcde2fce11ad&cmd=games&leagues=111&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560445884408"
        url2 = "https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112006797045150796266_1560445884377&hid=bdb01c9f736f2e641984fcde2fce11ad&cmd=games&leagues=97&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560445884409"
        url3 = "https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112006797045150796266_1560445884377&hid=bdb01c9f736f2e641984fcde2fce11ad&cmd=games&leagues=284&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560445884410"
        url4 = "https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112006797045150796266_1560445884377&hid=bdb01c9f736f2e641984fcde2fce11ad&cmd=games&leagues=285&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560445884412"
        urls = [{'bet':'MLB ALTERNATIVE RUNLINES','url':url1},{'bet':'MLB TEAM TO SCORE FIRST IN GAME','url':url2},{'bet':'MLB WILL THERE BE A SCORE IN THE 1ST INNING','url':url3},{'bet':'MLB TOTAL HITS+RUNS+ERRORS IN THE GAME','url':url4}]
        headers = {'scheme':'https','user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36'}
        
        batch_rows = []
        for url in urls:

            source = requests.get(url['url'],headers=headers).text
            pos1 = source.find("(")
            source = source[pos1+1:len(source)-2]
            json_data = json.loads(source)

            data_list = []
            game_json = {}
            game_list = []

            flag = False
            title = ''
            for game in json_data["games"]:
                lines = game["lines"]
                ab = True
                try:
                    ab_f = game["ab"]
                except Exception:
                    ab = False
                    pass
                if ab == True:
                    if flag == True:
                        game_json["data"] = game_list
                        data_list.append(game_json)
                    else:
                        flag = True
                    title = game["htm"]
                    if title == '':
                        title = game["vtm"]
                    continue
                elif len(lines) > 0:
                    line = lines[0]
                    first_team = game["vtm"] + " " +game["vpt"]
                    second_team = game["htm"] + " " + game["hpt"]
                    first_ml = line["voddsh"]
                    second_ml = line["hoddsh"]
                    first_total = line["ovh"]
                    second_total = line["unh"]
                    first_spread = line["vsprdh"]
                    second_spread = line["hsprdh"]

                    match_json = {}
                    match_json["Date"] = game["gmdt"]
                    match_json["Time"] = game["gmtm"]
                    match_json["Game"] = title.strip()
                    match_json["Team"] = first_team.strip()
                    match_json["Bet"] = url['bet']
                    match_json["Money Line"] = first_ml.strip()
                    match_json["Total"] = first_total.strip()
                    match_json["Run Line"] = first_spread.strip()
                    match_json["Website"] = "Bet Phoenix"
                    batch_rows.append(match_json)

                    match_json = {}
                    match_json["Date"] = game["gmdt"]
                    match_json["Time"] = game["gmtm"]
                    match_json["Game"] = title.strip()
                    match_json["Team"] = second_team.strip()
                    match_json["Bet"] = url['bet']
                    match_json["Money Line"] = second_ml.strip()
                    match_json["Total"] = second_total.strip()
                    match_json["Run Line"] = second_spread.strip()
                    match_json["Website"] = "Bet Phoenix"
                    batch_rows.append(match_json)
        self.clear_airtable()
        self.airtable.batch_insert(batch_rows)


def main():
    betphoenix = Betphoenix()
    betphoenix.scrape()
    
if __name__ == "__main__":main()
