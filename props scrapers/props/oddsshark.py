import requests
from bs4 import BeautifulSoup
import json
from datetime import datetime
from airtable import Airtable

class Oddsshark:
    def __init__(self):
        with open("setting.json") as f:
            settings = json.load(f)
            api_key = settings["api key"]
            base_key = settings["baseball"]
        self.airtable = Airtable(base_key, "Bookmaker", api_key=api_key)

    def clear_airtable(self):
        pages = self.airtable.get_all()
        for page in pages:
            self.airtable.delete(page["id"])

    def scrape(self):
        url = "https://www.bookmaker.eu/live-lines/baseball"
        headers = {'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36','scheme':'https'}
        source = requests.get(url,headers=headers).text
        soup = BeautifulSoup(source, 'lxml')
        content = soup.find('div',{'class':'sport'})

        data_json = {}
        data_list = []
        game_json = {}
        game_list = []

        games = content.findAll('div',{'class':'league'})

        for game in games:
            title = game.find('h2').text
            game_list = []
            game_json = {}
            game_json["title"] = title.strip()

            matches = game.findAll('div',{'class':'matchup'})

            for match in matches:
                cur_time = ''
                try:
                    cur_time = match.find('li',{'class':'time'}).text
                except Exception:
                    continue

                vTeam = match.find('div',{'class':'vTeam'})
                first_team = vTeam.find('div',{'class':'team'}).text
                first_spread = vTeam.find('div',{'class':'spread'}).text
                first_spread = first_spread.replace("-","")
                first_total = vTeam.find('div',{'class':'total'}).text
                first_ml = vTeam.find('div',{'class':'money'}).text

                hTeam = match.find('div',{'class':'hTeam'})
                second_team = hTeam.find('div',{'class':'team'}).text
                second_spread = hTeam.find('div',{'class':'spread'}).text
                second_spread = second_spread.replace("-","")
                second_total = vTeam.find('div',{'class':'total'}).text
                second_ml = vTeam.find('div',{'class':'money'}).text

                match_json = {}
                match_json["time"] = cur_time.strip()

                match_json["first rot"] = ''
                match_json["first team"] = first_team.strip()
                match_json["first spread"] = first_spread.strip()
                match_json["first ml"] = first_ml.strip()
                match_json["first total"] = first_total.strip()

                match_json["second rot"] = ''
                match_json["second team"] = second_team.strip()
                match_json["second spread"] = second_spread.strip()
                match_json["second ml"] = second_ml.strip()
                match_json["second total"] = second_total.strip()
                game_list.append(match_json)

            game_json["data"] = game_list
            data_list.append(game_json)
        now = datetime.now()
        data_json["date"] = str(now.year) + "-" + str(now.month) + "-" + str(now.day)
        data_json["games"] = data_list

        return data_json

    def push_airtable(self):
        result_json = self.scrape()
        self.clear_airtable()
        for game in result_json["games"]:
            title = game["title"]
            push_row = {}
            push_row["TEAM-PITCHER"] = title
            self.airtable.insert(push_row)
            data = game["data"]
            for match in data:
                push_row = {}
                push_row["TIME"] = match["time"]
                push_row["TEAM-PITCHER"] = match["first team"]
                push_row["SPREAD"] = match["first spread"]
                push_row["TOTAL"] = match["first total"]
                push_row["MLINE"] = match["first ml"]
                print(push_row)
                self.airtable.insert(push_row)

                push_row = {}
                push_row["TEAM-PITCHER"] = match["second team"]
                push_row["SPREAD"] = match["second spread"]
                push_row["TOTAL"] = match["second total"]
                push_row["MLINE"] = match["second ml"]
                print(push_row)
                self.airtable.insert(push_row)

def main():
    oddsshark = Oddsshark()
    oddsshark.clear_airtable()
    oddsshark.push_airtable()
    
if __name__ == "__main__":main()


