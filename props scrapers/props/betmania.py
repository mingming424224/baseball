import requests
from bs4 import BeautifulSoup
import json
from datetime import datetime
from airtable import Airtable

class Betmania:
    def __init__(self):
        with open("setting.json") as f:
            settings = json.load(f)
            api_key = settings["api key"]
            base_key = settings["mlb data"]
        self.airtable = Airtable(base_key, "Bet Mania Props", api_key=api_key)
    def clear_airtable(self):
        pages = self.airtable.get_all()
        for page in pages:
            self.airtable.delete(page["id"])
    
    def scrape(self):
        urls = [
            {'bet':'MLB TEAM TOTALS','url':'https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112024191543520208625_1560539735451&hid=39c7f91c104319aa89968481b60d43ff&cmd=games&leagues=90&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560539735492'},
            {'bet':'MLB ALTERNATIVE RUNLINES','url':'https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112024191543520208625_1560539735451&hid=39c7f91c104319aa89968481b60d43ff&cmd=games&leagues=111&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560539735493'},
            {'bet':'MLB TEAM TO SCORE FIRST IN GAME','url':'https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112024191543520208625_1560539735451&hid=39c7f91c104319aa89968481b60d43ff&cmd=games&leagues=97&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560539735494'},
            {'bet':'MLB WILL THERE BE A SCORE IN THE 1ST INNING','url':'https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112024191543520208625_1560539735451&hid=39c7f91c104319aa89968481b60d43ff&cmd=games&leagues=284&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560539735496'},
            {'bet':'MLB TOTAL HITS+RUNS+ERRORS IN THE GAME','url':'https://api.securecashiersystem.ag/js/Lines.js?jsoncallback=jQuery1112024191543520208625_1560539735451&hid=39c7f91c104319aa89968481b60d43ff&cmd=games&leagues=285&nhlLine=A&mblLine=A&lineStyle=E&language=0&_=1560539735497'}
        ]

        batch_rows = []
        for url in urls:
            source = requests.get(url['url']).text
            pos1 = source.find("(")
            source = source[pos1+1:len(source)-2]
            json_data = json.loads(source)

            for game in json_data["games"]:
                lines = game["lines"]
                if len(lines) > 0:
                    line = lines[0]
                    first_team = game["vtm"] +" "+ game["vpt"]

                    second_team = game["htm"] + " "+ game["hpt"]
                    first_ml = line["voddsh"]
                    second_ml = line["hoddsh"]
                    first_total = line["ovh"]
                    second_total = line["unh"]
                    first_spread = line["vsprdh"]
                    second_spread = line["hsprdh"]

                    item = {}
                    item["Date"] = game["gmdt"]
                    item["Time"] = game["gmtm"]
                    item["Team"] = first_team.strip()
                    item["Bet"] = url['bet']
                    item["Money Line"] = first_ml.strip()
                    item["Total"] = first_total.strip()
                    item["Run Line"] = first_spread.strip()
                    item["Website"] = "Bet Mania"
                    batch_rows.append(item)
                    print(item)

                    item = {}
                    item["Date"] = game["gmdt"]
                    item["Time"] = game["gmtm"]
                    item["Team"] = second_team.strip()
                    item["Bet"] = url['bet']
                    item["Money Line"] = second_ml.strip()
                    item["Total"] = second_total.strip()
                    item["Run Line"] = second_spread.strip()
                    item["Website"] = "Bet Mania"
                    batch_rows.append(item)
                    print(item)
        self.clear_airtable()
        self.airtable.batch_insert(batch_rows)

    
def main():
    betmania = Betmania()
    betmania.scrape()
    
if __name__ == "__main__":main()





