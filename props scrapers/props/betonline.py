import requests
from bs4 import BeautifulSoup
import json
from datetime import datetime
from airtable import Airtable

class Sportsbetting:
    def __init__(self):
        with open("setting.json") as f:
            settings = json.load(f)
            api_key = settings["api key"]
            base_key = settings["mlb data"]
        self.airtable = Airtable(base_key, "BetOnline Props", api_key=api_key)
    def clear_airtable(self):
        pages = self.airtable.get_all()
        for page in pages:
            self.airtable.delete(page["id"])
    def scrape(self):
        urls = [{'bet':'MLB Series','url':'https://www.betonline.ag/sportsbook/baseball/mlb-series'},{'bet':'Adj Run Line','url':'https://www.betonline.ag/sportsbook/baseball/adj-run-line'},{'bet':'Alt Run Line','url':'https://www.betonline.ag/sportsbook/baseball/alt-run-line'},{'bet':'R+H+E','url':'https://www.sportsbetting.ag/sportsbook/baseball/r-h-e'}]
        headers = {'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36','scheme':'https'}
        for url in urls:
            source = requests.get(url['url'],headers=headers).text
            soup = BeautifulSoup(source, 'lxml')

            contentBody = soup.find('div',{'id':'contentBody'})
            date = contentBody.find('tbody',{'class':'date'}).text
            pos = date.find('-')
            date = date[0:pos]
            table = contentBody.find('table',{'class':'league'})
            tbodies = table.findAll('tbody')
            date_id = tbodies[0]['id']
            
            batch_rows = []
            for tbody in tbodies[1:]:
                temp_class = tbody['class']
                if len(temp_class) > 1:
                    break

                first_tr = tbody.find('tr',{'class':'firstline'})
                first_tds = first_tr.findAll('td')          

                cur_time = first_tds[0].text

                first_rot = first_tds[1].text
                first_team_name = first_tds[2].text
                first_run_line  = first_tds[5].text + " " + first_tds[6].text
                first_money_line = first_tds[9].text
                first_total_runs = first_tds[12].text + " " + first_tds[13].text + " " + first_tds[14].text

                other_tr = tbody.find('tr',{'class':'otherline'})
                other_tds = other_tr.findAll('td')

                other_rot = other_tds[0].text
                other_team_name = other_tds[1].text
                other_run_line = other_tds[4].text + " " + other_tds[5].text
                other_money_line = other_tds[8].text
                other_total_runs = other_tds[11].text + " " + other_tds[12].text + " " + other_tds[13].text

                push_row = {}
                push_row["Date"] = date.strip()
                push_row["Time"] = cur_time.strip()
                push_row["Bet"] = url['bet']
                push_row["Team"] = first_team_name.strip()
                push_row["Run Line"] = first_run_line.strip()
                push_row["Total Runs"] = first_total_runs.strip()
                push_row["Money Line"] = first_money_line.strip()
                push_row["Website"] = "BetOnline"
                batch_rows.append(push_row)
                print(push_row)

                push_row = {}
                push_row["Date"] = date.strip()
                push_row["Bet"] = url['bet']
                push_row["Time"] = cur_time.strip()
                push_row["Team"] = other_team_name.strip()
                push_row["Run Line"] = other_run_line.strip()
                push_row["Total Runs"] = other_total_runs.strip()
                push_row["Money Line"] = other_money_line.strip()
                push_row["Website"] = "BetOnline"
                batch_rows.append(push_row)
                print(push_row)
            self.clear_airtable()
            self.airtable.batch_insert(batch_rows)

def main():
    sportsbetting = Sportsbetting()
    sportsbetting.scrape()
    
if __name__ == "__main__":main()


        


