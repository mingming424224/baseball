import requests
from bs4 import BeautifulSoup
import json
from datetime import datetime
from airtable import Airtable
import time
import re

class Abcislands:
    def __init__(self):
        with open("setting.json") as f:
            settings = json.load(f)
            api_key = settings["api key"]
            base_key = settings["mlb data"]
        self.airtable = Airtable(base_key, "ABC Islands Props", api_key=api_key)

    def clear_airtable(self):
        pages = self.airtable.get_all()
        for page in pages:
            self.airtable.delete(page["id"])

    def get_fraction(self,frac_str):
        result =  re.search('&frac(.*);', frac_str)
        spread1 = frac_str
        if result:
            spread = result.group(1)
            spread1 =  frac_str.replace("&frac"+result.group(1)+";"," "+spread[0]+"/" + spread[1])
        return spread1

    def scrape(self):
        headers = {'content-type':'application/json'}
        data  = {"IdPlayer": 0,"LineStyle": "E","LeagueList":[140]}
        
        source = requests.post("https://betslipapi.isppro.net/api/Guest/GetNewLines",json=data,headers=headers).text
        json_data = json.loads(source)
        if len(json_data) == 0:
            print([])
            print("no data")
            #exit
        games = json_data[0]['Games']

        data_json = {}
        data_list = []
        game_json = {}
        game_list = []
        game_json["title"] = json_data[0]['Description']

        for game in games:
            match_json={}
            gmdt = game['Gmdt']
            match_json["time1"] = gmdt[0:4] +"-"+gmdt[4:6]+"-"+gmdt[6:]
            match_json["time2"] = game['Gmtm']
            line = game['Lines'][0]
            match_json["first rot"] = game['Vnum']
            match_json["second rot"] = game['Hnum']
            match_json["first team"] = game['Vtm'] + " " + game["Vpt"]
            match_json["second team"] = game['Htm'] + " " + game["Hpt"]
            match_json["game"] = game['Gdesc']
            if line['Vsprdh'].strip() != '':
                match_json["first spread"] =self.get_fraction(line['Vsprdh'].strip())
            else:
                match_json["first spread"] = ''

            if line['Hsprdh'].strip() != '':
                match_json["second spread"] = self.get_fraction(line['Hsprdh'].strip())
            else:
                match_json["second spread"] = ''

            match_json["first ml"] = line['Voddsh']
            match_json["second ml"] = line['Hoddsh']
            if line['Ovh'].strip() != '':
                match_json["first total"] = self.get_fraction(line['Ovh'].strip())
            else:
                match_json["first total"] = ''
            if line['Unh'].strip() != '':
                match_json["second total"] = self.get_fraction(line['Unh'].strip())
            else:
                match_json["second total"] = ''

            game_list.append(match_json)
        game_json["data"] = game_list
        data_list.append(game_json)
        now = datetime.now()
        data_json["date"] = str(now.year) + "-" + str(now.month) + "-" + str(now.day)
        data_json["games"] = data_list

        return data_json

    def push_airtable(self):
        result_json = self.scrape()
        print(result_json)
        batch_rows = []
        for match in result_json["games"][0]["data"]:
            push_row = {}

            push_row["Date"] = match["time1"]
            push_row["Time"] = match["time2"]
            push_row["Game"] = match["game"]
            push_row["Team"] = match["first team"]
            push_row["Bet"] = "GAME PROPS"
            push_row["Run Line"] = match["first spread"]
            push_row["Total"] = match["first total"]
            push_row["Mline"] = match["first ml"]
            push_row["Website"] = "ABC Islands"
            print(push_row)
            batch_rows.append(push_row)

            push_row["Date"] = match["time1"]
            push_row["Time"] = match["time2"]
            push_row["Game"] = match["game"]
            push_row["Team"] = match["second team"]
            push_row["Bet"] = "GAME PROPS"
            push_row["Run Line"] = match["second spread"]
            push_row["Total"] = match["second total"]
            push_row["Mline"] = match["second ml"]
            push_row["Website"] = "ABC Islands"
            print(push_row)
            batch_rows.append(push_row)
        self.clear_airtable()
        self.airtable.batch_insert(batch_rows)

def main():
    abcislands = Abcislands()
    abcislands.push_airtable()
    
if __name__ == "__main__":main()

    
