import requests
from bs4 import BeautifulSoup
import json
import time
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options 
from airtable import Airtable

class Bovada:
    def __init__(self):
        with open("setting.json") as f:
            settings = json.load(f)
            api_key = settings["api key"]
            base_key = settings["mlb data"]
        self.airtable = Airtable(base_key, "Bovada Props", api_key=api_key)
    def clear_airtable(self):
        pages = self.airtable.get_all()
        for page in pages:
            self.airtable.delete(page["id"])
    def scrape(self):
        headers = {'scheme':'https','user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36'}
        urls = {
            'https://www.bovada.lv/services/sports/event/v2/events/A/description/baseball/mlb?marketFilterId=def&liveOnly=true&lang=en',
            'https://www.bovada.lv/services/sports/event/v2/events/A/description/baseball/mlb?marketFilterId=705&preMatchOnly=true&lang=en',
            'https://www.bovada.lv/services/sports/event/v2/events/A/description/baseball/mlb?marketFilterId=706&preMatchOnly=true&lang=en',
            'https://www.bovada.lv/services/sports/event/v2/events/A/description/baseball/mlb?marketFilterId=301&preMatchOnly=true&lang=en',
            'https://www.bovada.lv/services/sports/event/v2/events/A/description/baseball/mlb?marketFilterId=302&preMatchOnly=true&lang=en',
            'https://www.bovada.lv/services/sports/event/v2/events/A/description/baseball/mlb?marketFilterId=team-totals&preMatchOnly=true&lang=en',
            }
        batch_rows = []

        for url in urls:
            source = requests.get(url,headers=headers).text
            json_data = json.loads(source) 
            for event in json_data[0]["events"]:
                title = event["description"]
                start_time = event["startTime"]
                competitors = event["competitors"]
                first_team = competitors[1]["name"]
                second_team = competitors[0]["name"]
                groups = event["displayGroups"]
                bet = groups[0]["description"]
                markets = groups[0]["markets"]
                first_runline = ''
                try:
                    first_runline = markets[0]["outcomes"][0]["price"]["handicap"] + "("+ markets[0]["outcomes"][0]["price"]["handicap"] + ")"
                except Exception:
                    pass
                second_runline = ''
                try:    
                    second_runline = markets[0]["outcomes"][1]["price"]["handicap"] + "("+ markets[0]["outcomes"][1]["price"]["handicap"] + ")"
                except Exception:
                    pass
                first_ml = ''
                second_ml = ''
                try:
                    first_ml = markets[1]["outcomes"][0]["price"]["american"]
                except Exception:
                    pass
                try:
                    second_ml = markets[1]["outcomes"][1]["price"]["american"]
                except Exception:
                    pass

                first_total = ''
                second_total = ''
                try:
                    first_total = markets[2]["outcomes"][0]["price"]["handicap"] + "(" + markets[2]["outcomes"][0]["price"]["american"] + ")"
                except Exception:
                    pass
                try: 
                    second_total = markets[2]["outcomes"][1]["price"]["handicap"] + "(" + markets[2]["outcomes"][1]["price"]["american"] + ")"
                except Exception:
                    pass  
                time1 = datetime.fromtimestamp(int(start_time)/1000)
                match_json = {}
                match_json["Date"] = str(time1.today().year) +"-"+ str(time1.today().month) +"-"+ str(time1.today().day) 
                match_json["Time"] = str(time1.now().hour) +":"+str(time1.now().minute) + ":" + str(time1.now().second) 
                match_json["Game"] = title
                match_json["Team"] = first_team
                match_json["Bet"] = bet
                match_json["Money Line"] = first_ml
                match_json["Total"] = first_total
                match_json["Run Line"] = first_runline
                match_json["Website"] = "Bovada"
                batch_rows.append(match_json)
                print(match_json)
                
                match_json = {}
                match_json["Date"] = str(time1.today().year) +"-"+ str(time1.today().month) +"-"+ str(time1.today().day) 
                match_json["Time"] = str(time1.now().hour) +":"+str(time1.now().minute) + ":" + str(time1.now().second) 
                match_json["Game"] = title
                match_json["Team"] = second_team
                match_json["Bet"] = bet
                match_json["Money Line"] = second_ml
                match_json["Total"] = second_total
                match_json["Run Line"] = second_runline
                match_json["Website"] = "Bovada"
                batch_rows.append(match_json)
                print(match_json)
        self.clear_airtable()
        self.airtable.batch_insert(batch_rows)


def main():
    bovada = Bovada()
    bovada.scrape()
    
if __name__ == "__main__":main()




