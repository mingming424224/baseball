import requests
from bs4 import BeautifulSoup
import json
import time
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options 

class Betpop:
    pass
    def scrape(self):
        url = "https://www.betpop.eu/sportsbook"
        chrome_options = Options()
        #chrome_options.add_argument("--headless") 
        prefs = {'profile.managed_default_content_settings.images':2}
        chrome_options.add_experimental_option("prefs", prefs)
        chrome_path = r"chromedriver.exe" 
        driver = webdriver.Chrome(chrome_path,chrome_options=chrome_options)
        driver.set_window_position(-10000,0)
        driver.get(url)
        time.sleep(1)

        lis = driver.find_elements_by_class_name("panel-default")
        for li in lis:
            sport_name = li.find_element_by_class_name("sportsName").text
            sport_name = sport_name.strip()
            if sport_name == "Baseball":
                li.find_element_by_tag_name("a").click()
                time.sleep(1)
                driver.find_element_by_xpath("""//*[@id="1225516150"]/div/ul/li[1]/a""").click()
        try:
            element_present = EC.presence_of_element_located((By.ID,"gamesList"))
            WebDriverWait(driver, 10).until(element_present)
            time.sleep(3)
        except Exception:
            print("timeout!")
        
        container = driver.find_element_by_id("gamesList")
        prop_bts = container.find_elements_by_class_name("searchProps")
        for prop_bt in prop_bts:
            prop_bt.click()
            time.sleep(2)
        batch_rows = []
        soup = BeautifulSoup(driver.page_source, 'lxml')
        games = soup.findAll('div',{'class':'game'})
        for game in games:
            try:
                date_str = game.find('div',{'class':'date'}).text
                date_array= date_str.split(",")
                date = date_array[0]
                time1 = date_array[1]
                teams = game.findAll('span',{'class':'strong'})
                first_team = teams[0].text
                second_team = teams[1].text
                team_id = game.find('button',{'class':'searchProps'})["data-target"]
                team_id = team_id.replace("#","")
                prop_container = soup.find("div",{'id':'props_'+team_id})
                prop_rows = prop_container.findAll('div',{'class':'row'})
                for prop_row in prop_rows:
                    if prop_row.parent["class"][0] == "props-row":
                        title = prop_row.find("div",{'class':'props-header'}).text
                        lines = prop_row.findAll('div',{'class':'line'})
                        for line in lines:
                            bet = line.find('div',{'class':'col-xs-9'}).text
                            odds = line.find('label',{'class':'raised'}).text
                            item = []
                            item.append(date.strip())
                            item.append(time1.strip())
                            item.append(title.strip())
                            item.append(first_team.strip()  +" vs " + second_team.strip())
                            item.append(bet.strip())
                            item.append("")
                            item.append("")
                            item.append("")
                            item.append(odds.strip())
                            item.append("BetPop")
                            batch_rows.append(item)
                            print(item)
            except Exception:
                pass
        driver.quit()

        return batch_rows
def main():
    betpop = Betpop()
    betpop.scrape()
    
if __name__ == "__main__":main()




