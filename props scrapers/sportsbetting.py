import requests
from bs4 import BeautifulSoup
import json

class Sportsbetting:
    def __init__(self):
        pass
    def scrape(self):
        urls = [{'bet':'MLB Series','url':'https://www.sportsbetting.ag/sportsbook/baseball/mlb-series'},{'bet':'Adj Run Line','url':'https://www.sportsbetting.ag/sportsbook/baseball/adj-run-line'},{'bet':'Alt Run Line','url':'https://www.sportsbetting.ag/sportsbook/baseball/alt-run-line'},{'bet':'Grand Salami','url':'https://www.sportsbetting.ag/sportsbook/baseball/grand-salami'},{'bet':'R+H+E','url':'https://www.sportsbetting.ag/sportsbook/baseball/r-h-e'}]
        headers = {'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36','scheme':'https'}
        batch_rows = []
        for url in urls:
            source = requests.get(url['url'],headers=headers).text
            soup = BeautifulSoup(source, 'lxml')

            contentBody = soup.find('div',{'id':'contentBody'})

            table = contentBody.find('table',{'class':'league'})
            if table == None:
                continue
            tbodies = table.findAll('tbody')
            date_id = tbodies[0]['id']
            pos1 = date_id.find('MLB')

            date = contentBody.find('tbody',{'class':'date'}).text
            pos = date.find('-')
            date = date[0:pos]

            for tbody in tbodies[1:]:
                temp_class = tbody['class']
                if len(temp_class) > 1:
                    break

                first_tr = tbody.find('tr',{'class':'firstline'})
                first_tds = first_tr.findAll('td')          

                cur_time = first_tds[0].text
                first_team_name = first_tds[2].text
                first_run_line  = first_tds[5].text + " " + first_tds[6].text
                first_money_line = first_tds[9].text
                first_total_runs = first_tds[12].text + " " + first_tds[13].text + " " + first_tds[14].text

                first_run_line = first_run_line.strip()
                if first_run_line[0:1] == "+":
                    first_run_line = first_run_line[1:]

                other_tr = tbody.find('tr',{'class':'otherline'})
                other_tds = other_tr.findAll('td')
                other_team_name = other_tds[1].text
                other_run_line = other_tds[4].text + " " + other_tds[5].text
                other_money_line = other_tds[8].text
                other_total_runs = other_tds[11].text + " " + other_tds[12].text + " " + other_tds[13].text

                other_run_line = other_run_line.strip()
                if other_run_line[0:1] == "+":
                    other_run_line = other_run_line[1:]

                push_row = []
                push_row.append(date.strip())
                push_row.append(cur_time.strip())
                push_row.append("")
                push_row.append(first_team_name.strip())
                push_row.append(url['bet'])
                push_row.append(first_run_line.strip())
                push_row.append(first_money_line.strip())
                push_row.append(first_total_runs.strip())
                push_row.append("")
                push_row.append("SportsBetting")
                print(push_row)
                batch_rows.append(push_row)

                push_row = []
                push_row.append(date.strip())
                push_row.append(cur_time.strip())
                push_row.append("")
                push_row.append(other_team_name.strip())
                push_row.append(url['bet'])
                push_row.append(other_run_line.strip())
                push_row.append(other_money_line.strip())
                push_row.append(other_total_runs.strip())
                push_row.append("")
                push_row.append("SportsBetting")
                print(push_row)
                batch_rows.append(push_row)
        return batch_rows

def main():
    sportsbetting = Sportsbetting()
    sportsbetting.scrape()
    
if __name__ == "__main__":main()


        


