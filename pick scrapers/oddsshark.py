import requests
from bs4 import BeautifulSoup
import json

class Oddsshark:
    def __init__(self):
        pass
    def scrape(self):
        url = "https://www.oddsshark.com/mlb/computer-picks"
        source = requests.get(url).text
        soup = BeautifulSoup(source,'lxml')

        container = soup.find("div",{"id":"block-system-main"})
        games = container.findAll("table",{"class":"base-table"})
        values = []

        for game in games[:len(games)-1]:
            caption = game.find("div",{"class":"caption-wrapper"})
            names = caption.findAll("span",{"class":"name-long"})
            first_team = names[0].text
            second_team = names[1].text
            thead = game.find("thead")
            ths = thead.findAll("th")
            time0 = ths[0].text
            time0 = time0.replace("Matchup","")
            time0 = time0.replace("|","")
            time0 = time0.strip()
            time_array = time0.split("@")
            time1 = time_array[0]
            date1 = time_array[1]
            tbody = game.find("tbody")
            trs = tbody.findAll('tr')

            tds = trs[0].findAll('td')
            predicted_score = tds[0].text
            predicted_score = predicted_score.strip()
            to_win = tds[1].text
            total = tds[2].text
            item = []
            item.append(date1)
            item.append(time1)
            item.append(first_team)
            item.append(second_team)
            item.append(to_win)
            item.append(total)
            item.append("oddsshark")

            values.append(item)

            tds = trs[1].findAll('td')
            computer_pick = tds[0].text
            computer_pick = computer_pick.strip()
            to_win = tds[1].text
            total = tds[2].text

            item = []
            item.append(date1)
            item.append(time1)
            item.append(first_team)
            item.append(second_team)
            item.append(to_win)
            item.append(total)
            item.append("oddsshark")
            values.append(item)

            tds = trs[2].findAll('td')
            computer_pick = tds[0].text
            computer_pick = computer_pick.strip()
            to_win = tds[1].text
            total = tds[2].text

            item = []
            item.append(date1)
            item.append(time1)
            item.append(first_team)
            item.append(second_team)
            item.append(to_win)
            item.append(total)
            item.append("oddsshark")
            values.append(item)

            tds = trs[3].findAll('td')
            computer_pick = tds[0].text
            computer_pick = computer_pick.strip()
            to_win = tds[1].text
            total = tds[2].text

            item = []
            item.append(date1)
            item.append(time1)
            item.append(first_team)
            item.append(second_team)
            item.append(to_win)
            item.append(total)
            item.append("oddsshark")
            values.append(item)
        print(values)
        return values

