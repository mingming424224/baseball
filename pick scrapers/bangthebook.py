import requests
from bs4 import BeautifulSoup
import json

class Bangthebook:
    def __init__(self):
        pass
    def scrape(self):
        values = []
        url = "https://www.bangthebook.com/mlb/mlb-free-picks/"
        source= requests.get(url).text
        soup = BeautifulSoup(source,'lxml')
        container = soup.find('div',{'class':'blog_left'})
        games = container.findAll('article',{'class':'post'})
        for game in games:
            detail = game.find('div',{'class':'details'})
            datetime0 = detail.find('small').text
            time_array = datetime0.split("at")
            date0 = time_array[0]
            time0 = time_array[1]
            name = game.find('h3',{'itemprop':'name'})
            link = name.find('a')["href"]
            game_name = name.text

            sub_source = requests.get(link).text
            sub_soup = BeautifulSoup(sub_source,'lxml')
            con = sub_soup.find('div',{'class':'post_cont'})
            content = con.findAll("p")[3].text
            item = []
            item.append(time0.strip())
            item.append(date0.strip())
            item.append(game_name.strip())
            item.append('')
            item.append('')
            item.append(content)
            item.append("bangthebook")
            values.append(item)
        return values


    

