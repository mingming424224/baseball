import requests
from bs4 import BeautifulSoup
import json

class Docsports:
    def __init__(self):
        pass
    def scrape(self):
        values = []
        url = "https://www.docsports.com/free-picks/baseball/"
        source= requests.get(url).text
        soup = BeautifulSoup(source,'lxml')
        container = soup.find('div',{'class':'featured-article-content'})
        picks = container.findAll('div',{'itemprop':'about'})
        for pick in picks:
            datetime0 = pick.find('span',{'itemprop':'startDate'}).text
            time_array = datetime0.split("at")
            date0 = time_array[0]
            time0 = time_array[1]
            game_name = pick.find('a',{'itemprop':'url'}).text
            description = pick.find('p',{'itemprop':'description'}).text
            description = description.replace("Read More >>","")

            item = []
            item.append(time0.strip())
            item.append(date0.strip())
            item.append(game_name.strip())
            item.append('')
            item.append('')
            item.append(description.strip())
            item.append('docsports')
            print(item)
            values.append(item)
        return values
