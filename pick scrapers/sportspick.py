import requests
from bs4 import BeautifulSoup
import json

class Sportspick:
    def __init__(self):
        pass
        
    def scrape(self):
        url = "https://sports-pick.com/free-picks.php?LeagueID=mlb"
        source = requests.get(url).text
        soup = BeautifulSoup(source,'lxml')
        container = soup.find('section',{'class':'pt-4'})
        games = container.findAll('a',{'class':'list-group-item'})
        values = []

        for game in games:
            link = "https://sports-pick.com" + game["href"]
            sub_source = requests.get(link).text
            sub_soup = BeautifulSoup(sub_source,'lxml')
            sub_container = sub_soup.find('section',{'class':'pt-4'})
            title = sub_container.find('h2',{'class':'text-center'}).text
            title_array = title.split(" vs ")
            home = title_array[0]
            away = title_array[1]
            datetime0 = sub_container.find('h4').text
            datetime0 = datetime0.replace("MLB  Free Pick for","")
            description = sub_container.find('p',{'itemprop':'articleSection'}).text
            item = []
            item.append("")
            item.append(datetime0.strip())
            item.append(home.strip())
            item.append(away.strip())
            item.append("")
            item.append(description.strip())
            values.append(item)
            item.append("sports-pick")
            print(item)
        return values


