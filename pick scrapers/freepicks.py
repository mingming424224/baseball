import requests
from bs4 import BeautifulSoup
import json

class Freepicks:
    def __init__(self):
        pass
    def scrape(self):
        url = "https://freepicks.picksandparlays.net/more-free-picks/mlb-picks"
        page = 0
        flag = True
        values = []
        while flag == True:
            real_url = url + "?start=" + str(page)
            source = requests.get(real_url).text
            soup = BeautifulSoup(source,'lxml')
            container = soup.find('ul',{'id':'NewsList'})
            games = container.findAll("div",{'class':'newsArticle'})
            cur_date = games[0].find('div',{'class':'newsDate'}).text
            cur_date = cur_date.strip()
            for game in games:
                date1 = game.find('div',{'class':'newsDate'}).text
                date1 = date1.strip()
                if date1 == cur_date:
                    h3 = game.find('h3',{'class':'newsTitle'})
                    a_link = h3.find('a')['href']
                    a_link = "https://freepicks.picksandparlays.net" + a_link
                    sub_source = requests.get(a_link).text
                    sub_soup = BeautifulSoup(sub_source,'lxml')
                    article = sub_soup.find('div',{'id':'articleText'})
                    teams = article.select('p:nth-child(2) > span')[0]
                    team_text = teams.text
                    team_array = team_text.split("vs.")
                    first_team = team_array[0]
                    first_team = first_team.strip()
                    second_team = team_array[1]
                    second_team = second_team.strip()

                    h1s = article.findAll('h1')
                    time1 = h1s[0].text
                    time1 = time1.replace("MLB:","")
                    time1 = time1.strip()
                    odds = h1s[2].find('a').text
                    odds = odds.replace("Latest Odds :","")
                    odds = odds.strip()
                    item = []
                    item.append(time1)
                    item.append(date1)
                    item.append(first_team)
                    item.append(second_team)
                    item.append("")
                    item.append(odds)
                    item.append("freepicks")
                    values.append(item)
                    print(item)
                else:
                    flag = False
                    break
            page += 1
        return values


