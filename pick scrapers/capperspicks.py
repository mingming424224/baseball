import requests
from bs4 import BeautifulSoup
import json

class Capperspicks:
    def __init__(self):
        pass
    
    def scrape(self):
        url = "https://www.capperspicks.com/free-picks/mlb-baseball/"
        headers = {'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36','scheme':'https'}
        source = requests.get(url,headers=headers).text
        soup = BeautifulSoup(source,'lxml')
        container = soup.find('main',{'class':'content'})
        games = container.findAll('article',{'class':'category-mlb-baseball'})
        values = []
        for game in games:
            heading = game.find('header',{'class':'entry-header'})
            link = heading.find('a',{'class':'entry-title-link'})['href']
            sub_source = requests.get(link,headers=headers).text
            sub_soup = BeautifulSoup(sub_source,'lxml')
            content = sub_soup.find('main',{'class':'content'})
            article = content.find('article',{'class':'category-mlb-baseball'})
            title = article.find('h1',{'class':'entry-title'}).text
            team_array = title.split(" vs. ")
            home = team_array[0]
            away = team_array[1]
            pos = away.rfind('-')
            away = away[0:pos]
            datetime0 = article.find('time',{'class':'entry-time'})['datetime']
            time_array = datetime0.split("T")
            date0 = time_array[0]
            time0 = time_array[1]
            context = article.find('div',{'class':'entry-content'})
            ps = context.findAll('p')
            pick = ps[1].text

            item = []
            item.append(time0.strip())
            item.append(date0.strip())
            item.append(home.strip())
            item.append(away.strip())
            item.append('')
            item.append(pick.strip())
            item.append("capperspicks")
            values.append(item)
        return values


