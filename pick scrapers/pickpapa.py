import requests
from bs4 import BeautifulSoup
import json

def scrape():
    url = "https://pickpapa.com/mlb"
    headers = {'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36','scheme':'https'}
    source = requests.get(url,headers=headers).text
    soup = BeautifulSoup(source,'lxml')
    container = soup.find('div',{'id':'cards'})
    games = container.findAll('div',{'class':'buttons'})
    for game in games:
        a_tags = game.findAll('a')
        for a_tag in a_tags:
            link = a_tag["href"]
            print(link)
            sub_source = requests.get(link,headers=headers).text
            sub_soup = BeautifulSoup(sub_source,'lxml')
            article = sub_soup.find('div',{'class':'article-title'})
            span = article.find('span',{'class':'font-montreg'})
            teams = span.findAll('a',{'class':'quiet-link'})
            home = teams[0].text
            away = teams[1].text
            datetime0 = article.find('span',{'class':'team-game-start'}).text
            time_array = datetime0.split("\n")
            datetime0 = time_array[0]
            date_array = datetime0.split(" at ")
            date0 = date_array[0]
            time0 = date_array[1]
            description = sub_soup.find('div',{'class':'introduction'}).text

            item = []
            item.append(time0)
            item.append(date0)
            item.append(home)
            item.append(away)
            item.append('')
            item.append(description)
            item.append('pickpapa')
            print(item)

            
scrape()

