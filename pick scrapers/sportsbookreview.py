import requests
from bs4 import BeautifulSoup
import json

def scrape():
    url = "https://www.sportsbookreview.com/picks/mlb/"
    headers = {'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36','scheme':'https'}
    source = requests.get(url,headers=headers).text
    soup = BeautifulSoup(source,'lxml')
    container = soup.find('section',{'id':'mainSection'})
    articles = container.findAll('article')
    for article in articles:
        heading = article.find('h1')
        link = heading.find('a')['href']
        real_link = "https://www.sportsbookreview.com" + link
        sub_source = requests.get(real_link,headers=headers).text
        sub_soup = BeautifulSoup(sub_source,'lxml')
        content = sub_soup.find('div',{'class':'wrapper-content'})
        social = content.find('div',{'class':'wrapper-autor-date'})
        datetime0 = social.find('p',{'class':'publishDateDesktop'}).text
        pos = datetime0.rfind(' 2019 ')
        date0 = datetime0[0:pos+6]
        time0 = datetime0[pos+6:len(datetime0)]
        con = content.find('div',{'class':'article-content'})
        winner = con.find('em')
        pick = con.find('strong').text
        table_container = sub_soup.find('div',{'class':'table-container'})
        trs = table_container.findAll('tr')
        tds1 = trs[1].findAll('td')
        home = tds1[0].text
        td2 = trs[2].findAll('td')
        away = td2[0].text
        item = []
        item.append(time0)
        item.append(date0)
        item.append(home)
        item.append(away)
        item.append(winner)
        item.append(pick)
        item.append("sportsbookreview")
        print(item)

        
        print(link)

scrape()

