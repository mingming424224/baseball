import requests
from bs4 import BeautifulSoup
import json

class Sportschatplace:
    def __init__(self):
        pass

    def scrape(self):
        values = []
        url = "https://sportschatplace.com/picks/mlb"
        source = requests.get(url).text
        soup = BeautifulSoup(source,'lxml')
        container = soup.find('div',{'id':'block-sportschatplace-content'})
        games = container.findAll('div',{'class':'views-row'})
        for game in games:
            meta = game.find('div',{'class':'article-meta'})
            datetime0 = meta.find('span').text
            pos = datetime0.rfind(',')
            date0 = datetime0[0:pos]
            time0 = datetime0[pos+1:len(datetime0)]
            title = game.find('div',{'class':'article-title'})
            h2 = title.find('h2')
            a_tag = h2.find('a')
            game_name = a_tag.text
            name_array = game_name.split(" vs. ")
            home = name_array[0]
            away = name_array[1]
            pos1 = away.find("-")
            away = away[0:pos1]

            link ="https://sportschatplace.com" + a_tag["href"]


            sub_source = requests.get(link).text
            sub_soup = BeautifulSoup(sub_source,'lxml')
            sub_container = sub_soup.find('div',{'class':'article-data'})
            tbody = sub_container.find('tbody')
            trs = tbody.findAll('tr')
            td = trs[3].find('td')
            description = td.find('div').text

            item = []
            item.append(date0)
            item.append(time0)
            item.append(home)
            item.append(away)
            item.append('')
            item.append(description)
            item.append('sportschatplace')
            values.append(item)
        return values

        


