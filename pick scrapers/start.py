from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import requests
from bs4 import BeautifulSoup
import json
import time
from datetime import datetime
from oddsshark import Oddsshark
from vegas import Vegas
from freepicks import Freepicks
from bangthebook import Bangthebook
from docsports import Docsports
from sportspick import Sportspick
from sportschatplace import Sportschatplace
from mlbbaseballfreepicks import Mlbbaseballfreepicks
from cbssports import Cbssports

from capperspicks import Capperspicks
from betfirm import Betfirm

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

sheet_id = "1bZDdENMucVbe8ypDEFLzS2rnQtCuIoL6vZ2m8_UOTqg"

SAMPLE_RANGE_NAME = 'MLB Signals!A2:G'

class PickScraper():
    def __init__(self):
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.


        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        service = build('sheets', 'v4', credentials=creds)
        self.sheet = service.spreadsheets()
    def collect_values(self):
        value_list = []

        oddsshark = Oddsshark()
        result = oddsshark.scrape()
        value_list = value_list + result

        vegas = Vegas()
        result = vegas.scrape()
        value_list = value_list + result

        freepicks = Freepicks()
        result = freepicks.scrape()
        value_list = value_list + result

        bangthebook = Bangthebook()
        result = bangthebook.scrape()
        value_list = value_list + result

        docsports = Docsports()
        result = docsports.scrape()
        value_list = value_list + result

        sportspick = Sportspick()
        result = sportspick.scrape()
        value_list = value_list + result

        sportschatplace = Sportschatplace()
        result = sportschatplace.scrape()
        value_list = value_list + result

        mlbbaseballfreepicks = Mlbbaseballfreepicks()
        result = mlbbaseballfreepicks.scrape()
        value_list = value_list + result

        cbssports = Cbssports()
        result = cbssports.scrape()
        value_list = value_list + result

        capperspicks = Capperspicks()
        result = capperspicks.scrape()
        value_list = value_list + result

        betfirm = Betfirm()
        result = betfirm.scrape()
        value_list = value_list + result

        return value_list

    def start_scrape(self):
        while True:
            values = self.collect_values()
            body = {
                'values':values
            }
            self.sheet.values().clear(spreadsheetId=sheet_id, range=SAMPLE_RANGE_NAME, body={}).execute()
            time.sleep(1)
            self.sheet.values().append(spreadsheetId=sheet_id,range=SAMPLE_RANGE_NAME,valueInputOption="USER_ENTERED", body=body).execute()
            time.sleep(300)

def main():
    pickScraper = PickScraper()
    pickScraper.start_scrape()
    
if __name__ == "__main__":main()
    

