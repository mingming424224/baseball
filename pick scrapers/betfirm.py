import requests
from bs4 import BeautifulSoup
import json

class Betfirm:
    def __init__(self):
        pass
    def scrape(self):
        url = "https://www.betfirm.com/free-baseball-picks/"
        headers = {'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36','scheme':'https'}
        source = requests.get(url,headers=headers).text
        soup = BeautifulSoup(source,'lxml')
        container = soup.find('div',{'id':'affliatecontainer'})
        games = container.findAll('div',{'class':'free-pick-col'})
        values = []
        for game in games:
            info = game.find('div',{'class':'free-pick-game-info'})
            datetime0 = info.find('div',{'class':'free-pick-time'}).text
            time_array = datetime0.split(',')
            date0 = time_array[0]
            time0 = time_array[1]
            teams = info.find('div',{'class':'free-pick-game'}).text
            teams = teams.replace('MLB |','')
            team_array = teams.split(' vs ')
            home = team_array[0]
            away = team_array[1]

            description = info.find('div',{'class':'pick-result-success'}).text

            item = []
            item.append(time0.strip())
            item.append(date0.strip())
            item.append(home.strip())
            item.append(away.strip())
            item.append('')
            item.append(description.strip())
            item.append("betfirm")
            values.append(item)
        return values
