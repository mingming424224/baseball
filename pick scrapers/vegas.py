import requests
from bs4 import BeautifulSoup
import json

class Vegas:
    def __init__(self):
        pass
    def scrape(self):
        url = "http://www.vegasinsider.com/mlb/picks/free-pick/"
        source = requests.get(url).text
        soup = BeautifulSoup(source,'lxml')
        container = soup.find('div',{'id':'middlemlbPicksBox'})
        left_box = container.find('div',{'id':'mlbVsLeftBG'})
        right_box = container.find('div',{'id':'mlbVsRightBG'})

        values = []
        info_box = left_box.find('div',{'class':'mlbMatchUpInfoBox'})
        info_html = info_box.decode_contents()
        info_array = info_html.split("<br/>")
        teams = info_array[0]
        pos = teams.find("</font>")
        teams = teams[pos+7:len(teams)]
        teams = teams.strip()
        team_array = teams.split(" at ")
        first_team = team_array[1]
        second_team = team_array[0]

        date1 = info_array[1]
        pos = date1.find("</font>")
        date1 = date1[pos+7:len(date1)]
        date1 = date1.strip()

        time1 = info_array[2]
        pos = time1.find("</font>")
        time1 = time1[pos+7:len(time1)]
        time1 = time1.strip()

        pick_box = container.find('div',{'id':'freePickBox'}).text
        pick = pick_box.strip()

        item = []
        item.append(date1)
        item.append(time1)
        item.append(first_team)
        item.append(second_team)
        item.append("")
        item.append(pick)
        item.append("vegasinsider")
        values.append(item)

        info_box = right_box.find('div',{'class':'mlbMatchUpInfoBox'})
        info_html = info_box.decode_contents()
        info_array = info_html.split("<br/>")
        teams = info_array[0]
        pos = teams.find("</font>")
        teams = teams[pos+7:len(teams)]
        teams = teams.strip()
        team_array = teams.split(" at ")
        first_team = team_array[1]
        second_team = team_array[0]

        date1 = info_array[1]
        pos = date1.find("</font>")
        date1 = date1[pos+7:len(date1)]
        date1 = date1.strip()

        time1 = info_array[2]
        pos = time1.find("</font>")
        time1 = time1[pos+7:len(time1)]
        time1 = time1.strip()

        pick_box = container.find('div',{'id':'freePickBox'}).text
        pick = pick_box.strip()

        item = []
        item.append(date1)
        item.append(time1)
        item.append(first_team)
        item.append(second_team)
        item.append("")
        item.append(pick)
        item.append("vegasinsider")
        values.append(item)
        print(values)

        return values
