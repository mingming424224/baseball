import requests
from bs4 import BeautifulSoup
import json

class Cbssports:
    def __init__(self):
        pass
    def scrape(self):
        url = "https://www.cbssports.com/mlb/expert-picks/"
        headers = {'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36','scheme':'https'}
        source = requests.get(url,headers=headers).text
        soup = BeautifulSoup(source,'lxml')
        values = []
        try:
            container = soup.find('div',{'class':'picks-tbody'})
            games = container.findAll('div',{'class':'picks-tr'})
            datetime0 = soup.find('li',{'id':'ToggleContainer-buttons-button-2'}).text
            for game in games:
                mls = game.findAll('div',{'class':'moneyline'})
                teams = mls[0].findAll('div',{'class':'game-info-team'})
                home = teams[0].find('span',{'class':'team'}).text
                away = teams[1].find('span',{'class':'team'}).text
                winner = mls[3].find('div',{'class':'expert-spread'}).text
                pick = mls[3].find('div',{'class':'expert-ou'}).text
                item = []
                item.append(datetime0.strip())
                item.append('')
                item.append(home.strip())
                item.append(away.strip())
                item.append(winner.strip())
                item.append(pick.strip())
                item.append("cbssports")
                values.append(item)
                print(item)
        except Exception:
            pass
        return values


