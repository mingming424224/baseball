from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from bs4 import BeautifulSoup
import json
import time
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options 
from selenium.webdriver.support.ui import Select

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

sheet_id = "1bZDdENMucVbe8ypDEFLzS2rnQtCuIoL6vZ2m8_UOTqg"

# The ID and range of a sample spreadsheet.
SAMPLE_RANGE_NAME1 = 'MLB Moneyline Odds!A3:AE'
SAMPLE_RANGE_NAME2 = 'MLB Spreads!A3:BC'
SAMPLE_RANGE_NAME3 = 'MLB Total!A3:BC'

class Oddsshark():
    def __init__(self):
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.


        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        service = build('sheets', 'v4', credentials=creds)
        self.sheet = service.spreadsheets()

        chrome_options = Options()
        #    chrome_options.add_argument("--headless") 
        prefs = {'profile.managed_default_content_settings.images':2}

        chrome_options.add_experimental_option("prefs", prefs)
        chrome_path = r"chromedriver.exe" 
        self.driver = webdriver.Chrome(chrome_path,chrome_options=chrome_options)
        url = "https://www.oddsshark.com/mlb/odds"
        self.driver.get(url)

    def scrapeML(self):

        try:
            element_present = EC.presence_of_element_located((By.ID,"op-results"))
            WebDriverWait(self.driver, 10).until(element_present)
            time.sleep(3)
        except Exception:
            print("timeout!")
        

        soup = BeautifulSoup(self.driver.page_source, 'lxml')
        team_container = soup.find('div',{'class':'op-left-column-wrapper'})
        matches = team_container.findAll('div',{'class':['op-matchup-wrapper','op-separator-bar']})
        team_list = []
        cur_date = ""
        for match in matches:
            if match["class"][0] == "op-separator-bar":
                cur_date = match.text
                continue
            time1 = match.find('div',{'class':'op-matchup-time'}).text
            first_team = match.find('div',{'class':'op-team-top'}).text
            second_team = match.find('div',{'class':'op-team-bottom'}).text
            item = {}
            item["date"] = cur_date.encode("utf-8")
            item["time"] = time1.encode("utf-8")
            item["first team"] = first_team.encode("utf-8")
            item["second team"] = second_team.encode("utf-8")
            team_list.append(item)

            print(first_team)
            print(second_team)

        container = soup.find('div',{'id':'op-results'})

        results = container.findAll('div',{'class':'op-item-row-wrapper'})
        result_list = []
        for result in results:
            makers = result.findAll('div',{'class':'op-item-wrapper'})
            maker_list = []
            for maker in makers:
                spreads = maker.findAll('div',{'class':'op-spread'})
                first_spread = spreads[0].text
                second_spread = spreads[1].text
                item = {}
                item["first spread"] = first_spread.encode("utf-8")
                item["second spread"] = second_spread.encode("utf-8")
                maker_list.append(item)
            result_list.append(maker_list)
        self.driver.find_element_by_xpath("""//*[@id="op-odds-setting-selector"]/a""").click()
        time.sleep(2)
        self.driver.find_element_by_xpath("""//*[@id="odds-page-content-top"]/div[2]/div[2]/div[3]/label""").click()
        time.sleep(1)
        
        soup = BeautifulSoup(self.driver.page_source, 'lxml')
        fifth_list = []
        container = soup.find('div',{'id':'op-results'})
        results = container.findAll('div',{'class':'op-item-row-wrapper'})
        for result in results:
            makers = result.findAll('div',{'class':'op-item-wrapper'})
            maker_list = []
            for maker in makers:
                spreads = maker.findAll('div',{'class':'op-spread'})
                first_spread = spreads[0].text
                second_spread = spreads[1].text
                item = {}
                item["first spread"] = first_spread.encode("utf-8")
                item["second spread"] = second_spread.encode("utf-8")
                maker_list.append(item)
            fifth_list.append(maker_list)


        counter = 0
        values = []
        for team_item in team_list:
            item1 = []
            item2 = []

            item1.append(team_item["date"])
            item1.append(team_item["time"])
            item2.append(team_item["date"])
            item2.append(team_item["time"])
            item1.append(team_item["first team"])
            item2.append(team_item["second team"])
            idx = 0
            for result in result_list[counter]:
                item1.append(fifth_list[counter][idx]["first spread"])
                item1.append(result["first spread"])
                item2.append(fifth_list[counter][idx]["second spread"])
                item2.append(result["second spread"])
                idx += 1
            values.append(item1)
            values.append(item2)
            counter += 1

        print(values)

        body = {
            'values':values
        }

        return body

    def scrape(self):
        try:
            element_present = EC.presence_of_element_located((By.ID,"op-results"))
            WebDriverWait(self.driver, 10).until(element_present)
            time.sleep(3)
        except Exception:
            print("timeout!")
        

        soup = BeautifulSoup(self.driver.page_source, 'lxml')
        team_container = soup.find('div',{'class':'op-left-column-wrapper'})
        matches = team_container.findAll('div',{'class':['op-matchup-wrapper','op-separator-bar']})
        team_list = []
        cur_date = ""
        for match in matches:
            if match["class"][0] == "op-separator-bar":
                cur_date = match.text
                continue
            time1 = match.find('div',{'class':'op-matchup-time'}).text
            first_team = match.find('div',{'class':'op-team-top'}).text
            second_team = match.find('div',{'class':'op-team-bottom'}).text
            item = {}
            item["date"] = cur_date.encode("utf-8")
            item["time"] = time1.encode("utf-8")
            item["first team"] = first_team.encode("utf-8")
            item["second team"] = second_team.encode("utf-8")
            team_list.append(item)

            print(first_team)
            print(second_team)

        container = soup.find('div',{'id':'op-results'})

        results = container.findAll('div',{'class':'op-item-row-wrapper'})
        result_list = []
        for result in results:
            makers = result.findAll('div',{'class':'op-item-wrapper'})
            maker_list = []
            for maker in makers:
                spreads = maker.findAll('div',{'class':'op-spread'})
                first_spread = spreads[0].text
                second_spread = spreads[1].text
                prices = maker.findAll('div',{'class':'spread-price'})
                first_price = prices[0].text
                second_price = prices[1].text
                item = {}
                item["first spread"] = first_spread.encode("utf-8")
                item["second spread"] = second_spread.encode("utf-8")
                item["first price"] = first_price.encode("utf-8")
                item["second price"] = second_price.encode("utf-8")
                maker_list.append(item)
            result_list.append(maker_list)

        self.driver.find_element_by_xpath("""//*[@id="op-odds-setting-selector"]/a""").click()
        time.sleep(2)
        self.driver.find_element_by_xpath("""//*[@id="odds-page-content-top"]/div[2]/div[2]/div[3]/label""").click()
        time.sleep(1)
        
        soup = BeautifulSoup(self.driver.page_source, 'lxml')
        fifth_list = []
        container = soup.find('div',{'id':'op-results'})
        results = container.findAll('div',{'class':'op-item-row-wrapper'})
        for result in results:
            makers = result.findAll('div',{'class':'op-item-wrapper'})
            maker_list = []
            for maker in makers:
                spreads = maker.findAll('div',{'class':'op-spread'})
                first_spread = spreads[0].text
                second_spread = spreads[1].text
                prices = maker.findAll('div',{'class':'spread-price'})
                first_price = prices[0].text
                second_price = prices[1].text
                item = {}
                item["first spread"] = first_spread.encode("utf-8")
                item["second spread"] = second_spread.encode("utf-8")
                item["first price"] = first_price.encode("utf-8")
                item["second price"] = second_price.encode("utf-8")
                maker_list.append(item)
            fifth_list.append(maker_list)

        counter = 0
        values = []
        for team_item in team_list:
            item1 = []
            item2 = []

            item1.append(team_item["date"])
            item1.append(team_item["time"])
            item2.append(team_item["date"])
            item2.append(team_item["time"])
            item1.append(team_item["first team"])
            item2.append(team_item["second team"])
            idx = 0
            for result in result_list[counter]:
                item1.append(fifth_list[counter][idx]["first spread"])
                item1.append(fifth_list[counter][idx]["first price"])
                item1.append(result["first spread"])
                item1.append(result["first price"])
                item1.append(fifth_list[counter][idx]["second spread"])
                item1.append(fifth_list[counter][idx]["second price"])
                item2.append(result["second spread"])
                item2.append(result["second price"])
                idx += 1
            values.append(item1)
            values.append(item2)
            counter += 1
        print(values)
        body = {
            'values':values
        }
        return body


    def change_line(self):
        self.driver.refresh()
        body1 = self.scrapeML()
        self.sheet.values().clear(spreadsheetId=sheet_id, range=SAMPLE_RANGE_NAME1, body={}).execute()
        self.sheet.values().append(spreadsheetId=sheet_id,range=SAMPLE_RANGE_NAME1,valueInputOption="USER_ENTERED", body=body1).execute()
        self.driver.refresh()
        self.driver.find_element_by_xpath("""//*[@id="op-sticky-header-wrapper"]/div[1]/div/a""").click()
        time.sleep(1)
        self.driver.find_element_by_xpath("""//*[@id="op-sticky-header-wrapper"]/div[1]/div/ul/li[2]/a""").click()
        body2 = self.scrape()
        self.sheet.values().clear(spreadsheetId=sheet_id, range=SAMPLE_RANGE_NAME2, body={}).execute()
        self.sheet.values().append(spreadsheetId=sheet_id,range=SAMPLE_RANGE_NAME2,valueInputOption="USER_ENTERED", body=body2).execute()
        self.driver.refresh()
        self.driver.find_element_by_xpath("""//*[@id="op-sticky-header-wrapper"]/div[1]/div/a""").click()
        time.sleep(1)
        self.driver.find_element_by_xpath("""//*[@id="op-sticky-header-wrapper"]/div[1]/div/ul/li[3]/a""").click()
        body3 = self.scrape()
        self.sheet.values().clear(spreadsheetId=sheet_id, range=SAMPLE_RANGE_NAME3, body={}).execute()
        self.sheet.values().append(spreadsheetId=sheet_id,range=SAMPLE_RANGE_NAME3,valueInputOption="USER_ENTERED", body=body3).execute()

    def start(self):
        while True:
            self.change_line()
            time.sleep(300)

def main():
    oddsshark = Oddsshark()
    oddsshark.start()
    
if __name__ == "__main__":main()
