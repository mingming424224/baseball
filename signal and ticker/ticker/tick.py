from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from bs4 import BeautifulSoup
import json
import time
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options 
from selenium.webdriver.support.ui import Select

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

#sheet_id = "1xoZNu7V5YMEF_TU1AmYGLdvViGyviufQa6yJXemy204"

# The ID and range of a sample spreadsheet.
SAMPLE_RANGE_NAME1 = 'Sheet1!A2:H'
SAMPLE_RANGE_NAME2 = 'Sheet2!A2:B'
SAMPLE_RANGE_NAME3 = 'Sheet3!A2:B'

class Ticker:

    def __init__(self):
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        with open("setting.json") as f:
            settings = json.load(f)
            self.spreadsheet_id = settings["sheet id"]
            self.user_name = settings["user name"]
            self.password = settings["user password"]


        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        service = build('sheets', 'v4', credentials=creds)
        self.sheet = service.spreadsheets()

        chrome_options = Options()
        chrome_options.add_argument("--headless") 
        prefs = {'profile.managed_default_content_settings.images':2}

        chrome_options.add_experimental_option("prefs", prefs)
        chrome_path = r"chromedriver.exe" 
        self.driver = webdriver.Chrome(chrome_path,chrome_options=chrome_options)
        url = "https://live2.wallstjesus.com/login"
        self.driver.get(url)
    def login(self):
        try:
            element_present = EC.presence_of_element_located((By.ID,"input_0"))
            WebDriverWait(self.driver, 50).until(element_present)
            time.sleep(1)
            self.driver.find_element_by_id("input_0").send_keys(self.user_name)
            self.driver.find_element_by_id("input_1").send_keys(self.password)
            self.driver.find_element_by_xpath("""/html/body/div[1]/md-sidenav/div[1]/div[3]/form/button""").click()
        except Exception:
            print("timeout!")

        try:
            element_present = EC.presence_of_element_located((By.ID,"btnAcceptPolicy"))
            WebDriverWait(self.driver, 30).until(element_present)
            self.driver.find_element_by_id("btnAcceptPolicy").click()
            time.sleep(1)
        except Exception:
            print("timeout!")

    def scrape(self):
        time.sleep(3)
        try:
            element_present = EC.presence_of_element_located((By.ID,"wiseguyAlertsTable"))
            WebDriverWait(self.driver, 30).until(element_present)
            time.sleep(5)
        except Exception:
            print("timeout!")

        flag = False
        while flag == False:
            soup = BeautifulSoup(self.driver.page_source, 'lxml')
            container = soup.find('div',{'id':'wiseguyAlertsTable'})
            try:
                table = container.find('table')
                trs = table.findAll('tr')
                if len(trs) > 2:
                    break
            except Exception:
                pass
            time.sleep(1)
        call_counter = 0
        put_counter = 0
        soup = BeautifulSoup(self.driver.page_source, 'lxml')
        container = soup.find('div',{'id':'wiseguyAlertsTable'})
        table = container.find('table')
        trs = table.findAll('tr')
        values = []
        for tr in trs:
            tds = tr.findAll('td')
            received = tds[1].text
            received = received.strip()
            sym = tds[2].text
            sym = sym.strip()
            exp = tds[3].text
            exp = exp.strip()
            strike = tds[4].text
            strike = strike.strip()
            ref = tds[5].text
            ref = ref.strip()
            ty_div = tds[6].find('div')
            ty = ty_div["aria-label"]
            ty = ty.strip()
            size = tds[7].text
            size = size.strip()
            sentiment = tds[8].text
            sentiment = sentiment.strip()
            if sentiment == "Long Call":
                call_counter += 1
            else:
                put_counter += 1
            item = []
            item.append(received.encode("utf-8"))
            item.append(sym.encode("utf-8"))
            item.append(exp.encode("utf-8"))
            item.append(strike.encode("utf-8"))
            item.append(ref.encode("utf-8"))
            item.append(ty.encode("utf-8"))
            item.append(size.encode("utf-8"))
            item.append(sentiment.encode("utf-8"))
            values.append(item)

            print(item)
        body = {
            'values':values
        }

        self.sheet.values().clear(spreadsheetId=self.spreadsheet_id, range=SAMPLE_RANGE_NAME1, body={}).execute()
        self.sheet.values().append(spreadsheetId=self.spreadsheet_id,range=SAMPLE_RANGE_NAME1,valueInputOption="USER_ENTERED", body=body).execute()
        unique_list = []
        for value in values:
            symbol = value[1]
            sent = value[7]
            check = 0
            for unique in unique_list:
                if unique[0] == symbol:
                    unique[1] += 1
                    check += 1
                    if sent == "Long Call":
                        unique[2] += 1
                    else:
                        unique[3] += 1
            if check == 0:
                item = []
                item.append(symbol) 
                item.append(1)
                if sent == "Long Call":
                    item.append(1)
                    item.append(0)
                else:
                    item.append(0)
                    item.append(1)
                unique_list.append(item)
        body1 = {
            'values':unique_list
        }
        
        self.sheet.values().clear(spreadsheetId=self.spreadsheet_id, range=SAMPLE_RANGE_NAME2, body={}).execute()
        self.sheet.values().append(spreadsheetId=self.spreadsheet_id,range=SAMPLE_RANGE_NAME2,valueInputOption="USER_ENTERED", body=body1).execute()
            
        # body2 = {
        #     'values':[
        #         [call_counter,put_counter],
        #     ]
        # }

        # self.sheet.values().clear(spreadsheetId=self.spreadsheet_id, range=SAMPLE_RANGE_NAME3, body={}).execute()
        # self.sheet.values().append(spreadsheetId=self.spreadsheet_id,range=SAMPLE_RANGE_NAME3,valueInputOption="USER_ENTERED", body=body2).execute()
        
    def start(self):
        while True:
            self.driver.refresh()
            self.scrape()
            time.sleep(50)
def main():
    ticker = Ticker()
    ticker.login()
    ticker.start()

    
if __name__ == "__main__":main()
